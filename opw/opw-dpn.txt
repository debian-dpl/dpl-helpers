Debian Outreach Program for Women matching fund 2013

In an effort to get more women involved in FOSS, the Debian Project will be participating in the GNOME Foundation's Outreach Program for Women (OPW) [1], which is an internship program similar to Google's Summer of Code program. 

Unlike in Google Summer of Code, OPW participants do not need to be students and non-coding projects are available. In addition to coding, projects include such tasks as translation, graphic design, documentation, bug triage and marketing. 

In order to aid this effort, one of Debian's generous sponsors has offered to start a matching fund in USD to fund Debian's participation in OPW.

For details, and to give a donation, please see our "Donate now" page: http://debian.ch/opw2013

Please do think about getting involved and sharing your ideas with us, to help us make OPW an even more useful program for Debian in the future.

If you wish to apply as an intern participant, please review the available coding [2] and non-coding [3] internships and follow the instructions for application [4].

[1] - https://live.gnome.org/OutreachProgramForWomen
[2] - https://wiki.debian.org/SummerOfCode2013/Projects
[3] - https://wiki.debian.org/OutreachProgramForWomen/NonCodingProjects
[4] - https://wiki.debian.org/OutreachProgramForWomen
