#!/usr/bin/perl
# Copyright 2012 Nathan Handler <nhandler@debian.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use Tie::File;
use DateTime;	#libdatetime-perl
use DateTime::Duration;
use DateTime::Format::ICal; #libdatetime-format-ical-perl

my $file="meetings.ics";

die("Usage: $0 timestamp\nExample: $0 1352829600\n")	if($#ARGV+1 != 1);

my $now = DateTime->now();
my $start = DateTime->from_epoch(epoch => $ARGV[0]);

my $dur = DateTime::Duration->new(hours => 1);
my $end = $start + $dur;

print "New Meeting: " . DateTime::Format::ICal->format_datetime($start) . " - " . DateTime::Format::ICal->format_datetime($end) . "\n";

my @lines = ();
tie @lines, 'Tie::File', "$file" or die "Failed to tie $file!\n";
$#lines -= 1;	#Get rid of last line 'END:VCALENDAR'
my @event = (
	'BEGIN:VEVENT',
	'STATUS:CONFIRMED',
	'SEQUENCE:0',
	'DTSTART:' . DateTime::Format::ICal->format_datetime($start),
	'DTEND:' . DateTime::Format::ICal->format_datetime($end),
	'DTSTAMP:' . DateTime::Format::ICal->format_datetime($now),
	'UID:' . DateTime::Format::ICal->format_datetime($start) . '@dpl.debian.org',
	'DESCRIPTION:Agenda: http://deb.li/3iNJG',
	'Location:#debian-dpl',
	'SUMMARY:Debian DPL Helpers Meeting',
	'TRANSP:OPAQUE',
	'END:VEVENT',
	'END:VCALENDAR',
);

push (@lines, @event);

untie @lines;
