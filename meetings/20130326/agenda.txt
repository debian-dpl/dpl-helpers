#    -*- mode: org -*-

#+TITLE: DPL helpers - working agenda
#+DATE:  [2013-03-26 Tue 18:00]

* next meeting
** date -d @1365526800 (April 9) ?
*** UTC time set back 1 hour to keep local time constant for zack

* action items from last meeting
** TODO zack to contact debconf-team to draft a "job description" for delegation
** TODO lucas to wrap-up the salvaging/orphaning thread and submit dev-ref patch
** TODO moray to check with debconf team how/if to deal with debconf invited talks
** TODO bgupta to write temple reply to trademark requests that don't actually need approval, as they are nominative uses of the trademark
** TODO Diziet Make progress on inbound trademark policy
** TODO bgupta to ping mishi one week from today, if we haven't heard back

* additional topics for discussion
** new DPL term starts April 17, 2013
   - all candidates declared interest in continuing dpl-helpers
   - any specific step we should take to make the migration smooth?
** Debian participation into GNOME OPW
   - see: https://lists.debian.org/debian-women/2013/03/msg00013.html
   - zack will be happy to approve budget for 1 intern
   - needed help for extra (i.e. non-GSoC) topics and mentors
** official Debian blog coming soon
   - maybe even tonight, thanks to ana, MadameZou, and DSA
   - help welcome with the delegation
** discussions started on debian-sponsors-discuss
   - see
     https://lists.alioth.debian.org/mailman/listinfo/debian-sponsors-discuss
   - then died out :-/
** reminders/meetings are prone to be forgotten
   - any idea about / volunteer to automate them?
