17:11:02 <h01ger> #startmeeting
17:11:02 <MeetBot> Meeting started Tue Oct  9 17:11:02 2012 UTC.  The chair is h01ger. Information about MeetBot at http://wiki.debian.org/MeetBot.
17:11:02 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
17:11:03 <zack> suitable for voting (if needed), or otherwise suitable as reference for the future
17:11:05 <h01ger> #chair zack
17:11:05 <MeetBot> Current chairs: h01ger zack
17:11:12 <zack> so that we don't need to go in circle in the future
17:11:23 <h01ger> now we have public logs
17:11:27 <Diziet> zack: Would you like me to take the action item to discuss this on -project ?
17:11:39 <zack> Diziet: the process part you mean?
17:11:40 <Diziet> Yes.
17:11:44 <Diziet> We have to do that first.
17:11:45 <zack> sure, it'd be great
17:11:51 <Diziet> First we have to have the process then we can follow it :-).
17:12:02 <zack> I think the "writing the position more formally" can go in parallel
17:12:09 <zack> because it looks like it'd be needed anyhow
17:12:18 <Diziet> I think the answer is that DPL statements are fine provided they're not controversial.  If they appear to be then the DPL can go do it via GR, or objectors can make their own GR.
17:12:23 <Diziet> zack: Sure.
17:12:28 <zack> (agreed)
17:12:36 <lucas> which raises a question: when doing such tasks, do we need to introduce ourselves as being part of the DPL helpers cabal?
17:12:45 <zack> do you or anyone else would also like to pick up the drafting part or not?
17:12:50 <zack> lucas: I'll come to that in a sec
17:12:55 <Diziet> lucas: Certainly we should say "I have volunteered to Zack to help sort this out" surely ?
17:13:01 <Diziet> zack: I am willing to do so.
17:13:06 <zack> Diziet: ok great
17:13:17 <zack> (could someone do the two #action thingies for Diziet ?)
17:13:37 <zack> lucas: I haven't revealed the names, because it's you who have volunteerd and not me delegated or anything
17:13:39 <Diziet> #action Diziet start conversation on -project about dpl statement process
17:13:49 <zack> but surely there's nothing wrong in saying "I'm helping zack on $foo"
17:13:50 <h01ger> #save
17:13:56 <zack> it might also encourage others to do the same ;-)
17:14:08 <Diziet> #action Diziet draft more formal statement re trademarks and discuss on -project
17:14:15 <zack> but, to be clear: ATM I don't expect that "hat" to give any extra power or moral authority or anything
17:14:17 <Diziet> #action Diziet draft more formal statement re trademarks and discuss on -project, apropos of https://lists.debian.org/debian-project/2012/02/msg00073.html
17:14:38 <zack> #action algernon to review draft DMCA policy and adapt for the mentors.d.o use case
17:14:44 <zack> (backlog)
17:14:47 <Diziet> I assume that when such a thing needs to become formal, we would recommend to zack some particular decision and zack would agree or not.
17:15:01 <zack> indeed
17:15:06 <Diziet> dmca policy> Is this (necessarily) secret ?
17:15:15 <zack> Diziet: no, it is not
17:15:19 * h01ger apologies for being too tired to be very constructive..
17:15:23 <Diziet> Can we have a url for the log then ? :-)
17:15:35 <Diziet> (For me too of course since I couldn't find it...)
17:15:36 <zack> sure, I don't think anything should be secret
17:15:39 <h01ger> Diziet, http://meetbot.debian.net/debian-dpl/2012/
17:15:52 <zack> I just avoided to anounce this first meeting publicly, because I supposed some of you could be shy :-P
17:15:56 <h01ger> (its updated on #save and at #endmeeting)
17:16:00 <Diziet> h01ger: I meant a url of the dmca policy draft for the benefit of the people reading the log.
17:16:03 <zack> but ideally, they should become public meeting, announced for participations by others, etc.
17:16:08 <zack> pretty much as the tech-ctte ones
17:16:17 <zack> Diziet: ah, sorry!
17:16:35 <zack> so, the draft doc I got is, I think, a policy SFLC prepared for some other project
17:16:36 <Diziet> I didn't find it in my quick skim of the debian-mentor archives.
17:16:50 <zack> it's probably public for that project anyhow, but I should better ask for permission about that
17:16:56 <Diziet> zack: So we're not able to make it public at this time ?  I guess that's OK.
17:16:59 <Diziet> Right.
17:17:02 <zack> that's correct, yes
17:17:14 <Diziet> Someone needs to ask that other project and/or SFLC.
17:17:17 <zack> but I'll work to be sure we can make it public ASAP
17:17:22 <Diziet> That someone does not need to be zack so who is volunteering ?
17:17:29 <Diziet> Ideally someone involved already.
17:17:41 <zack> I'd rather keep that for the moment
17:17:43 <Diziet> OK
17:17:48 <zack> just because I've been the only contact point for SFLC up to now
17:18:00 <zack> should be enlarged, but I don't want to shock them either pointing to too many people at once :)
17:18:04 <Diziet> #action zack to ask SFLC / other project re making DMCA policy draft public
17:18:22 <zack> regarding procedures, we shold probably also eventually have a list, pseudopackage in the BTS, etc
17:18:26 <zack> but that's premature at this point, imho
17:18:45 <zack> move on to next point?
17:19:00 <zack> (we probably won't go through all of them to keep the meeting short, but that's not a big deal either)
17:19:32 <zack> next one is debian-companies, it'd probably help to read the buglog where I've put the rationale of the initiative
17:19:47 <zack> one liner: there's stuff debian volunteers won't do to promote debian in the corporate world. ever
17:19:52 <Diziet> buglog> # ?
17:19:57 <zack> let's have a companies use group that takes care of that
17:20:02 <zack> #650082
17:20:13 <zack> (it's in the titanpad too)
17:20:54 <zack> next action there is on the communication side
17:21:09 <zack> send out a "call for participation" to invate comapnies with a strategic interest in Debian to join
17:22:31 * h01ger is interested (a bit) in the companies topic and more in LTS but i'm also noticing atm that i'm really not able to follow this meeting much atm, too tired - and 5min break after work didnt refresh me..
17:22:53 <zack> h01ger: no worries, there'll be other chances in the future :-P
17:23:06 <lucas> stupid question probably answered in the bug log: is the list archive public?
17:23:19 <zack> lucas: no, it is not, and that has been a controversial part in the list creation
17:23:32 <zack> but accepted by listmasters
17:23:35 <lucas> ok, I should read the bug log ;)
17:23:54 <zack> it is not public because the initial participants expressed that as a requirement, and I think it makes sense for this specific use case
17:24:10 <zack> my moral justification on why that's acceptable is that we're basically only offering _hosting_ for that user group
17:24:31 <zack> but I think the list of subscribe companies should be made public
17:24:40 <Diziet> zack: Right.
17:24:48 <zack> (althoug I'm not sure that currently possible with our mailing list sw)
17:24:51 <Diziet> I think though that you should mention this whole thing on -project.
17:25:04 <Diziet> Having read the bug I'm comfortable with it.
17:25:10 <zack> well, I've mentioned that on d-d-a in the past
17:25:19 <Diziet> Have you ?  OK, I must have thought "oh whatever"
17:25:24 <Diziet> Then I guess it's fine.
17:25:49 <zack> so, once again, my issue is that I'm not sure it will work
17:25:58 <zack> i.e. if we'll reach a critical mass of interested companies
17:26:12 <zack> so, yes, we could have a thread on -project, but asking what?
17:26:31 <zack> what I'd like to do is try it out, and for that we need to invite companies
17:26:36 <zack> -> hence the news item
17:26:38 <Diziet> I just meant to make sure no-one accuses you of acting secretly but if it was in a dda bits mail then fine.
17:26:43 <Diziet> Fine.
17:26:49 <Diziet> What's the next action / decision ?
17:26:54 <zack> ah, I see
17:26:57 <moray> zack: as you make the user group comparison, wouldn't it work best if someone who would qualify for regular membership tried to push it?  or do you think it's better to have a neutral non-qualifying Debian person?
17:27:24 <zack> moray: yes, it'd be better, but i've failed to convince anyone of the early participants to drive that :-/
17:27:31 <lucas> in the announcement, maybe it's a good idea to point to the dda bit so that people being surprised can realize they should have read dda ;)
17:28:12 <zack> Diziet: next TODO item is whether someone would like to pick up drafting a news item
17:28:20 <zack> and we've two press team members on the channel ;-)
17:28:29 <zack> but it could stay lingering there too, it's no big deal
17:29:02 <zack> the point after that, is mine and hardly splittable any further: I just need to finalize delegation texts
17:29:08 <zack> and could probably due that this week directly
17:29:24 <lucas> I fear that you are the only one with enough context on -companies to announce that properly
17:30:01 <zack> but I'm not particularly good at writing announcements, so I was hoping in a 4-handed-work with someone completing me on that side
17:30:11 <zack> anyway
17:30:16 <moray> lucas: but the press team (rather than specific individual) could certainly be asked to put something together, that's its job
17:30:47 <Diziet> zack: How about you write some rough draft with the relevant info and send it to the press team to be polished/rewritten/whatever and sent back to you for approval ?
17:31:03 <zack> I could write an outline of the content, and mail press asking for a draft
17:31:19 <zack> than, before publishing, the draft could be posted to -project to stay on the safe side, as you suggested
17:31:30 <zack> moray: how does that sound?
17:31:33 <moray> yes, that sounds sensible (modulo the problem of press@ being impossible to read due to spam)
17:31:40 <zack> eh :-)
17:31:54 <zack> #action zack to write an outline of the -companies announcement and mail it to press@d.o
17:31:59 <zack> #save
17:32:12 <zack> next item is the trademark policy
17:32:17 <zack> I've a 2nd draft to mail to -project
17:32:30 <zack> the only action is reviewing the differences and summarizing them
17:32:37 <zack> to include the summary in the mail for discussion
17:32:50 <Diziet> This is _outbound_ trademark policy for our trademarks, rather than the previous thing which was _inbound_ trademarks.
17:33:04 <zack> Diziet: correct, and *thanks* for the very clear terminology
17:33:06 <zack> consider it adopted!
17:33:41 <zack> so, the next action I could offlead there is reviewing the diff and summarize it
17:33:46 <zack> if anyone cares enough about this
17:34:07 <zack> (but that's not a particularly sexy task either, there are more down the list ;-))
17:34:41 <Maulkin> (For info, I stayed out of the companies discussion as $work may well be interested in applying if it's eased up)
17:34:56 <zack> Maulkin: right, your company would indeed be a good candidate
17:35:23 <zack> on the DMCA front, there are two sub-tasks: one has been picked up by algernon,
17:35:36 <zack> the other (package description for the libdvdcss installer) is blocked waiting for a draft from SFLC
17:35:43 <zack> so noaction needed/possible there
17:36:04 <zack> the next one is interesting, and fairly typical of what's badly needed on debian lists
17:36:14 <zack> summarizing/driving to conclusion a couple of discussions
17:36:23 <zack> - salvaging packaging
17:36:28 <zack> - the "official" debian logo
17:36:49 <zack> anyone with a specific interest in any of those?
17:37:08 <lucas> I could try to work on the "salvaging packages" discussion
17:37:24 <lucas> (but I haven't read it yet)
17:37:45 <zack> lucas: ok, imho there there is a need of a good idea to do what Bart Martens has suggested
17:37:56 <zack> (i.e. re-using exisiting orphaning procedures)
17:38:15 <zack> but coupling it with something more sane than requiring other DDs to "ack" the "forced" orphaning
17:38:38 <zack> lucas: sounds like a good fit wrt your former work on devref :-P
17:39:06 <lucas> ok. however "october" sounds like hell to me, so it wouldn't hurt if someone else worked on it in parallel
17:40:05 <zack> lucas: just have a look at the thread, I'm confident that if you'll find it interesting you'll jump in right ahead
17:40:13 <zack> otherwise I'll re-assess it in a couple of weeks or so
17:40:25 <lucas> ok
17:40:30 <zack> #action lucas to look into the salvaging thread
17:40:55 <zack> the official logo part is way more useless, but a nice exercise in "consensus building" :-P
17:41:13 <zack> imho it's clearly a logo that nobody uses, but which theoretically could be useful
17:41:58 <moray> I fall into the "I don't see a problem with it existing" camp, so just ignoring it is also a viable path :)
17:42:22 <zack> moray: yeah, I fully understand that position. I guess it bothers me it's called the "official" logo, mostly
17:42:53 <zack> that's why the "issue" could probably be solved simply proposing a patch to the www people that renames it in something less pompous
17:42:57 <Diziet> zack: You could call it the "restricted" logo.
17:43:06 <moray> zack: but possibly any change should be a GR since the logos were adopted that way
17:43:07 <zack> Diziet: indeed, something like that
17:43:16 <Diziet> If you did that everyone would be happy, I guess.
17:43:22 * Maulkin is failing to see what the massive problem is with the current situation.
17:43:42 <Diziet> Maulkin: There are apparently people who think that any idea that Debian might restrict something is hideous.
17:43:43 <zack> well well well, look at this http://www.debian.org/vote/1999/raul-0.gif
17:43:48 <zack> the names are inverted, aren't them?
17:43:56 <Diziet> zack: Yes.
17:43:57 <moray> zack: there was another vote, IIRC, to swap them
17:44:02 <Diziet> As moray says.
17:44:13 <zack> ouch
17:44:22 <zack> lol, that's true
17:44:49 <moray> probably after people realised that it was difficult to restrict "leaving off the bottle" compared to restricting adding it
17:45:18 <zack> I'll guess I'll just propose renaming, giving credit to Diziet for the idea
17:45:32 <zack> arguably, the name is outside the GR, as long as we use them consistently with what we've done up to now
17:45:41 <moray> practically, changing the description on the website seems fine yes
17:45:50 <zack> #action zack to propose renaming the "official" logo to "restricted"
17:46:07 <zack> so, we're running out of time and I want to keep this short
17:46:14 <zack> I've 3 general comments to share:
17:46:39 <zack> 1) again: even if we don't advance on everything, it's not big deal, it's trying the model that matters
17:46:44 <zack> 2) feel free to propose topics!
17:46:59 <zack> there are some more general/long term plans at the end of the titanpad, think about them, and add yours if you fell like
17:47:02 <zack> fell like
17:47:18 <zack> 3) when do we meet again?
17:47:19 <zack> :)
17:47:31 <zack> (that's the part where I've to be pushy, sorry about that)
17:48:00 <zack> considering the time frame, monthly it's too far awawy, I'd like to have the next one in 2 weeks if you're available
17:48:01 <lucas> one thing that surprises me, looking at the TODO list, is the amount of (boring) legal stuff. Do you think it's due to your own interests in moving forward those topics, or generally part of the usual DPL workload?
17:48:06 <lucas> 3) -> doodle again?
17:48:16 <zack> lucas: I've asked myself that question too
17:48:24 <zack> my take is that there was a huge backlog
17:48:30 <zack> that nobody cared enough about to solve
17:48:44 <zack> topics like: swpats, trademarks, dvdcss
17:48:52 <zack> are all stuff we ragged under the carpet since, like, forever
17:49:19 <zack> I'm positive that once done, future DPLs will have less legal-ish stuff at hand
17:50:18 <zack> lucas: (3) I could doodle, yes, but tentatively would meeting in 2 weeks sounds reasonable to you?
17:50:23 <Diziet> Also most of the technical stuff is handled elsewhere in the project.
17:50:29 <lucas> same time in two weeks? yes
17:50:31 <Diziet> The DPL naturally accumulates legal and political stuff.
17:50:42 <moray> meeting in 2 weeks is quite likely possible for me, but I can't say definitely yet
17:50:54 <zack> sure, it's just tentative, I'll confirm via email
17:51:04 <Diziet> 2 weeks works for me.
17:51:09 <zack> do you want to announce the meeting more broadly, or prefer to postpone that to meeting+2 ?
17:51:17 <Diziet> But can I suggest that we should try to prepare the agenda a bit more thoroughly ?
17:51:28 <Diziet> Maybe that is a thing that can be delegated ?
17:51:33 <zack> Diziet: you're totally right, and I apologize for that
17:51:47 <zack> I've participated in IRC meeting, but never organized some, I could use some help, yes
17:51:51 <Diziet> Eg, looking up all the docs, bug#s, etc. so they are in the agenda in advance and we can do our reading before the meeting.
17:51:56 <Diziet> zack: OK sure I volunteer.
17:52:09 <lucas> [announcement] let's postpone that until we have a couple results to share?
17:52:11 <Diziet> I will bug you by email about.
17:52:25 <zack> lucas: sounds good to me
17:52:28 <Diziet> announcement> next dpl bits would be a sane place to mention it
17:52:32 <zack> Diziet: ok, great
17:52:39 <moray> zack: before I think better of it, out of the list of other topics I'd volunteer to help push forwards the local groups point -- I think I was silent when I saw list discussion, but I do think it's a good idea
17:52:45 <Diziet> #action Diziet to bug zack re next meeting agenda
17:53:06 <zack> moray: that's a very important topic, which I feel sad about not having pushed enough in the past ~2.5 years
17:53:16 <zack> it has both a policy and a technical (coding) part
17:53:27 <Diziet> Would it be OK if I asked owner@bugs for a pseudopackage and started to use the bts for tracking the todo list ?
17:53:45 <zack> the coding part would've been a perfectly sized GSoC project for a webdev, but requires first some thought at the policy part
17:53:55 <lucas> Diziet: isn't that too early?
17:54:02 <zack> Diziet: it is needed, yes, but I agree with lucas it's too early
17:54:14 <zack> in the sense that I still fear that in the end this effort will be abandoned
17:54:21 <zack> and we'll keep around a sort of stale BTS page
17:54:22 <Diziet> OK, fine.
17:55:01 <zack> anything else?
17:55:40 <zack> ok then
17:55:41 <zack> #endmeeting