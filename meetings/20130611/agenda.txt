#+TITLE: DPL helpers - working agenda
#+DATE:  [2013-06-11 Tue 17:00]

* next meeting
[2013-06-25 Tue 17:00] ? (date -d @1372179600) 
would be available: lucas, 
wouldn't be available: 

* New topics
** NEW discuss transfer of debian.mx to Debian TO
** NEW discuss if it makes sense to split out Trademark into seperate repo, or should we add new TM team members to dpl-helpers?
** NEW review status new TM team members
** NEW how to handle non-English debian.* domains, if they are transferred to Debian. debian.fr is an example. Currently we redirect to d.o and rely on cn. (Seems cn is not widely used).
** NEW dpl, or someone from dpl-helpers attend SPI meeting on the 13th?
** NEW Help Martin (auditor) draft specs for TO requirements (Martin has agreed for help offer)
** NEW should we ask deb-multimedia.org to change <title> (currently: "Debian Multimedia Packages". proposed: "[Unofficial] Multimedia Packages for Debian") ?

* Action items from last meeting (please update status and mention if discussion is needed. if not, we will just skip it during the meeting) -- no discussion needed?
** DONE lucas ping again on the SPI transactions issue
mail sent to board@spi on 2013-06-11
** DONE bgupta follow up with auditor@ regarding status of TO criteria I'll be helping Martin on the criteria also NMUed http://wiki.debian.org/Teams/Auditor/Organizations

** DONE bgupta to flesh out debian-sponsors wiki here http://wiki.debian.org/Fundraising, and ping list again to share. Discussions ongoing. Need to figure out best way to bring appropriate groups into the conversation
** DONE bgupta Write to Mishi@SFLC and confirm that no changes are required to TM policy if we register Logo Confirmed no changes required.
** DONE bgupta investigate full madrid costs initial costs for logo registration will come in at $3347. Every tenth year after that we are looking at $3915. US-only would be a lot less. I recomend we start with US only, since it is a prerequisite for Madrid. Also I should respond to -project thread.

** DONE lucas/bgupta to add header of criterias for adding things and general goal
** DONE bgupta commit list of debian.* domains to dpl-helpers repo and notify auditors about list
** TODO zack to answer on -cloud@ about general philosophical statements from Debian
** TODO moray to propose a more detailed process about the teams survey
** TODO moray to initiate work on paths into the project
** TODO Diziet make progress on inbound trademark policy
** TODO paultag do ics automailer
