16:59:02 <lucas> #startmeeting
16:59:02 <MeetBot> Meeting started Wed Oct  9 16:59:02 2013 UTC.  The chair is lucas. Information about MeetBot at http://wiki.debian.org/MeetBot.
16:59:02 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
16:59:11 <lucas> #topic roll call
16:59:12 <lucas> hi!
16:59:18 <bgupta> here
16:59:39 <rafw> i am here too.
16:59:40 <RichiH> oi
16:59:49 * zack is around-ish
17:00:58 <lucas> ok, I'm not sure if we are expecting someone else, so let's start
17:01:01 <lucas> #topic next meeting
17:01:07 <lucas> proposal is:
17:01:08 <lucas> $ date -ud @1382547600
17:01:08 <lucas> Wed Oct 23 17:00:00 UTC 2013
17:01:17 <lucas> (two weeks from now)
17:01:20 * highvoltage is here too
17:01:35 <rafw> might not be there. not sure now.
17:01:36 <zack> k
17:01:51 * RichiH will be busy at linuxcon.eu, but yah
17:01:58 <lucas> rafw: would any other (close) option be better for you?
17:02:24 <rafw> no, i have plan to go to a conference in Luxenbourg.
17:02:30 <rafw> But i am not sure now.
17:02:41 <rafw> I will do my best to attend.
17:02:57 <lucas> rafw: ok, if you want to drop by Nancy on your way there for a beer, just ping me ;)
17:03:09 <rafw> oki :)
17:03:18 <lucas> #agreed next meeting date -ud @1382547600
17:03:26 <lucas> #topic DPL todo list
17:03:47 <lucas> since I have live updates to http://people.debian.org/~lucas/todo.txt, I didn't copy/paste it to the agenda
17:04:20 <zack> so, I've posted the binary-throw-away (and source-only) upload summary to -helpers
17:04:29 <zack> not sure if you wanted to discuss that now or later on as action item
17:04:43 <zack> either way, it's done, and it looks like the next action in the DPL todo list has already been updated
17:04:55 <lucas> the private part is 1 item, + 20 mails (10 threads)
17:05:15 <lucas> yes
17:05:38 <lucas> the dak call for help went out in the DPL bits. we will see.
17:05:47 <zack> seen that, thanks
17:05:58 <lucas> I would have liked to have "easy tasks for beginners" in them, but apparently there's no such thing with dak ;)
17:06:06 <zack> *sadness*
17:06:31 <lucas> the big question about the TODO list is: is something missing?
17:07:11 <lucas> I think that we should also use those meetings to make sure that important things happening in the dark corners of Debian are noticed
17:07:22 <lucas> (you don't need to answer now, of course)
17:07:34 <RichiH> lucas: getting rid of money if debconf13 really needs to get rid of it?
17:07:45 <lucas> - [lucas] deal with DC13 surplus
17:07:47 <lucas> it's in it
17:07:57 <RichiH> ah, i grepped for debconf
17:07:58 <RichiH> sorry
17:07:59 <zack> I guess I've some info about the debbugs http submision, as it's based on a script of mine
17:08:04 <zack> but I need to look up the link
17:08:27 <lucas> ah yes. we discussed that with asheesh at debconf, and I didn't look for the link again
17:08:37 <lucas> zack: do you mind if I #action that to you?
17:08:42 * paultag sits down
17:08:43 <zack> lucas: sure, go ahead
17:08:54 <zack> I've been involved in some mail exchanges post-debconf
17:09:01 <lucas> #action zack find status of debbugs submissions over HTTP
17:09:59 <lucas> #topic new topics
17:10:09 <lucas> (let's also discuss new topics if you have any)
17:10:27 <zack> so, I've an idea I've been thinking about on and off: a (periodic) survey of debian developers (and maintainers maybe) to discover how many of them are paid to do debian work
17:10:42 <zack> I think it's important to know that, as it's related to our common "independence" claim
17:10:55 <zack> OTOH, having a survey which only asks that specific question seems a bit silly...
17:11:12 <zack> so if others have idea of what kind of useful information we could use to know from DDs, I'm all ears
17:11:15 <lucas> we would probably think of more questions
17:11:18 <zack> (this is nothing high priority, of course)
17:11:42 <zack> lucas: yes, the (meta- :))question is what we need to know of DDs to better drive debian?
17:11:51 <zack> that we don't know yet, that is
17:11:56 <RichiH> a (bi-)yearly survey may be nice, but it needs someone to do and to evaluate
17:12:11 <zack> RichiH: yep
17:12:16 <lucas> regarding paying DDs, it would be nice to distinguish between "employer knows I'm working on Debian during working hours and doesn't mind" and "contributing to Debian is part of my day job"
17:12:32 <bgupta> "How much time do you spend working on Debian Project related tasks?"
17:12:37 <highvoltage> zack: surely debian would still be independent even if all its developers were paid to work on it? (or am I misunderstanding the issue?)
17:12:48 <zack> highvoltage: it depends
17:12:56 <zack> if they're all working for different companies, arguably yes
17:12:59 <lucas> highvoltage: not if they were paid by the same company, for example
17:13:02 <zack> (that's the linux kernel model basically)
17:13:09 <zack> if not, what lucas said
17:13:14 <highvoltage> I'd be really surprised if the latter would be the case :)
17:13:30 <zack> highvoltage: I won't be surprised to find that a high percentage of dds working on debian work for canonical, for instance
17:13:33 <lucas> it was a common fear in early canonical times
17:13:46 <zack> it would be interesting to know how high that percentage is, though
17:14:18 <highvoltage> I know lots of folk at canonical and I don't think there are more than a dozen DDs employed there
17:14:27 <zack> we'll see :)
17:14:42 <paultag> highvoltage: but early on, it was a migh higer percent
17:14:43 <zack> lucas: I can take an action to draft a first list of interesting "things" we might want to know, if you want me to
17:14:46 <lucas> type of employer / field would be interesting too. e.g. using the linked in categories
17:14:50 <paultag> highvoltage: so it was assumed that % would continue
17:14:58 <highvoltage> paultag: yep, but a lot has changed in recent years
17:15:24 <paultag> (like being less desktop centric, for better or worse)
17:15:25 <zack> highvoltage: that's perception, and I agree with you, but data is something else
17:15:26 <lucas> #action zack to draft questions for a survey of DDs
17:15:32 <highvoltage> paultag: in the beginning, canonical pretty much hired any dd that was interested
17:15:47 <lucas> "biggest problems one sees in Debian" would be an interesting question, too
17:15:50 <zack> (it's more "things we want to know" to begin with, questions come later ,but ok, just nitpicking :))
17:15:53 <highvoltage> zack: *nod*, yes it will be interesting what the data says
17:16:04 <RichiH> zack: can you bounce those off of me as well? i think it's important to have a good set of questions if we bother people in general
17:16:19 <RichiH> (not saying you can't come up with good questions)
17:16:40 <zack> RichiH: sure, and no worries, I'm definitely not going to start mailing people before several rounds with this group
17:16:52 <RichiH> we will also need a realistic (and efficient) scheme to go through feedback
17:18:35 <lucas> ok, anything else?
17:19:03 <zack> (not from me)
17:19:21 <RichiH> neither
17:19:24 <rafw> nope
17:19:52 <lucas> ok
17:20:02 <lucas> #topic Action items from last meeting
17:20:20 <lucas> #topic ** TODO bgupta to follow up to TO email thread and cc dpl-helpers@
17:20:38 <lucas> *** Done. Probably need to expand the conversation? Discussion needed.
17:21:01 <lucas> so, I think that the most important question is: what do we want to do with this definition
17:21:18 <lucas> if we answer that one, it'll be easier to know what to write, and with how many details.
17:21:37 <zack> do we have a working draft somewhere?
17:22:05 <zack> (also, I note that the definition is in the constitution, what we want is probably something more like "minimal requirements" to be listed, or something such)
17:22:24 <zack> I remember having commented on list, but I've no idea whether the comments were addressed (even ditching them, of course :)) or not
17:22:48 <lucas> yes. one of the goal should be to clarify what we expect, so that people creating an organization with the aim of becoming a TO at some point know what to expect
17:23:08 <zack> ack
17:23:32 <lucas> not really, the last mail of the thread is http://lists.alioth.debian.org/pipermail/dpl-helpers/2013-September/000095.html
17:23:47 <lucas> (not really = we don't really have a working draft)
17:23:51 <lucas> (AFAIK)
17:24:19 <lucas> zack: from your POV, which organizations are currently TO?
17:24:29 <bgupta> we don't the thread is all.. without a kinda goals of this I can't really know which way to go.
17:24:38 <zack> I guess that's a question best asked to tbm, but certainly SPI, FFIS, debian.ch
17:25:00 <zack> and debian france looks good to become one
17:25:04 <bgupta> that list alligns with what auditors said when I asked them awhile back.
17:25:07 <lucas> there's a list at https://wiki.debian.org/Teams/Auditor/Organizations, that includes SPI, FFIS and debian.ch
17:25:26 <lucas> however, I'm not sure if debian.ch is really officially one
17:25:29 <bgupta> (I cleaned that wiki page up based on the feedback from auditors)
17:25:37 <zack> oh, I knew that page, but didn't notice the split between TO and others up to now
17:25:49 <zack> the distinction on that page matches my intuition
17:25:50 <zack> lucas: why not?
17:25:53 <lucas> nor if it meets all of the obvious requirements, e.g. access to bank listings
17:26:14 <zack> lucas: well, we don't have access to SPI bank accounts either
17:26:28 <lucas> that's not a reason for not putting that in requirements :)
17:26:35 <zack> oh, sure :)
17:26:41 <zack> I'm just saying that I don't see much of a difference
17:26:55 <zack> and I guess what you mean is "access without mediation to bank accounts"
17:27:05 <zack> on that front I think FFIS is the only one that qualifies
17:27:17 <lucas> mediation would be OK, I think, especially if automated
17:27:37 <zack> agreed
17:28:04 <zack> OTOH, as a treasurer of a non-profit (not related to software) I feel their pay, it's not necessarily easy to implement that
17:28:14 <zack> so I guess some periodic disclosure would be enough
17:28:21 <zack> maybe set a minimum period, e.g. quarterly?
17:28:42 <lucas> quarterly + on demand, maybe
17:28:51 <zack> make sense
17:29:15 <lucas> one of the ultimate goals of auditor@ is to be able to expose a "Debian budget". and if we can't consolidate info from various TOs at the same time, it's quite hard to do
17:29:29 <bgupta> When I tackled the TO definition, I was of the mind to make it useful for organizations thinking about persuing TO status, as well as useful to the project on having some level of consistency going forward on considering new TOs.
17:29:32 <lucas> of course, it might be that auditor@ should just do some double-accounting
17:29:34 <RichiH> requiring regular disclosure and leaving the method up to the TO would make sense, no?
17:30:38 <zack> lucas: the more stringent requirement is being able to answer the typical DPL question "how much (non-committed) money do we have *now*?"
17:30:51 <zack> because for the debian budget, that has a cadence too, and quarterly would be more than enough
17:30:55 <lucas> zack: but I wonder if it should be a question for auditor@, not TOs
17:30:57 <zack> most organizations publish only yearly budgets
17:31:10 <zack> lucas: I lost you, which question?
17:31:16 <bgupta> lucas: auditor can't do this without support from TOs
17:31:27 <lucas> "how much non-commited money do we have *now*?"
17:31:41 <RichiH> why not ask auditor@ for what they can work with and have them come up with requirements?
17:31:55 <lucas> bgupta: I'm actually not so sure. auditor@ is supposed to see all transactions
17:32:12 <zack> lucas: it depends on the level of freedom that TOs have
17:32:13 <lucas> bgupta: for some of them, not having an aggregated view really sucks, sure
17:32:28 <zack> it is possible for auditor to answer that question only if TOs have zero freedom in using money
17:32:38 <highvoltage> lucas: isn't there some sort of accounting software in use that keeps track of funds/costs?
17:32:39 <zack> (which is a possibility, of course)
17:33:02 <zack> highvoltage: it's not a problem of software :), but yes, there are aplenty, debian auditors use ledger-cli
17:33:27 <highvoltage> zack: so what's the problem? couldn't they get the balance(s) from there?
17:33:37 <zack> processes are the problem
17:33:41 <highvoltage> ah
17:33:42 <zack> each TOs have their own
17:33:50 <zack> and you need to integrate those with debian (auditor's) one
17:33:52 <RichiH> highvoltage: as i understand it "how to get the data in there"
17:34:25 <RichiH> zack: why not ask TOs to come up with an export that imports into ledger-cli cleanly?
17:34:40 <zack> RichiH: tbm has been writing several of those importers
17:34:50 <RichiH> ok
17:34:55 <lucas> zack: could we require TOs to have a separate Debian bank account, and notify auditor@ if they decide to use that bank account for something?
17:35:04 <rafw> I think we need centralization at one place of debian assets. Would that be possible ?
17:35:23 <lucas> rafw: no, that's part of the fun ;)
17:35:26 <rafw> :)
17:35:28 <zack> lucas: sure, we can, but that comes with a cost. I guess it would be enough to have separate earmarks (which is what debian france plans to do, IIRC)
17:35:31 <zack> anyway, to move forward
17:35:37 <bgupta> rafw: Not really, and I don't necessarily thing it's needed.
17:35:44 <zack> I think the requirement for being a TO should be vetted by auditors, at the very minimum
17:35:49 <zack> but we need a draft to start with
17:36:01 <lucas> zack: note that bgupta is part of auditor@ now
17:36:09 <zack> ah, right!
17:36:18 <bgupta> zack the issue so far is the drinitial draft I proposed to auditors incited no feedback
17:36:29 <zack> ping, maybe?
17:36:36 <bgupta> s/drinitial/initial/
17:36:44 <zack> and too bad I didn't realize that, as I've met tbm in Paris a few days ago
17:36:54 <lucas> bgupta: don't take it personal :)
17:37:06 <lucas> yeah, I talked to him too. he feels bad about behind on auditor@ stuff
17:37:32 <bgupta> ok
17:37:33 <zack> bgupta: but is that the draft commented upon?
17:37:46 <zack> if so, it would be great if you could integrate (or maybe rebuke) my comments
17:37:56 <lucas> bgupta: I currently have four outstanding items for auditor@: reimbursement procedures/RT, TO def, comments on DC13 surplus, hug as auditor@
17:37:59 <bgupta> ok will do…
17:38:12 <zack> great, thanks!
17:38:19 <lucas> bgupta: or maybe just hack the current wiki page based on feedback + discussion today?
17:38:20 <bgupta> #action bgupta respond to TO draft thread
17:39:06 <bgupta> #action start wiki page for collaborating on TO definition
17:39:27 <bgupta> (err forgot to say bgupta on that second one)
17:39:46 <lucas> #action bgupta improve https://wiki.debian.org/Teams/Auditor/Organizations based on TO definition discussions
17:39:57 <lucas> (I think that it could go to that page)
17:40:16 <lucas> ok, moving on
17:40:16 <lucas> ** TODO bgupta confirm the final stored currency for debian.ch paypal account
17:40:19 <lucas> *** Asked. Waiting for answer. No discussion needed.
17:40:23 <bgupta> Wait
17:40:28 <bgupta> I got an answer
17:40:36 <bgupta> or at least the start of one
17:42:00 <bgupta> CHF is the current default.. they can send other currencies to other orgs that have paypal accounts.. Debian.ch does have the ability to store USD and EURO bank balances, but currently when "withdrawing" from Paypal they can only do so in "home currency" of CHF. hug is investigating options.
17:43:19 <bgupta> e.g. for OPW we'll be able to send USD straight to Gnome's paypal in USD.
17:43:42 <bgupta> (Assuming that the money goes to Gnome Foundation
17:43:53 <lucas> for OPW, we might want to transfer from DC13
17:44:03 <bgupta> sure..
17:44:35 <lucas> ok, thanks for digging into this
17:44:44 <lucas> #topic ** TODO RichiH to look into the status of the press team, and advise lucas on how to update the current delegation
17:45:05 <RichiH> lucas: you have the mails cc, should i summarize for the benefit of the logs?
17:45:21 <lucas> I propose to move that point to email, as it's still under discussion with the press team
17:45:46 <lucas> there's a suggestion to reduce the requirements for sending press releases, and have something similar to dda
17:45:54 <RichiH> correct
17:46:20 <lucas> (pabs and Maulkin agree on that)
17:46:33 <RichiH> there's also a vibe of maybe dismantling -press as a group and relying on =publicity and individual DDs
17:46:35 <taffit> FWIW, that doesn’t scale with http://www.debian.org/News/
17:46:47 <RichiH> which is a tad meh, but that's what it feels like
17:47:04 <lucas> taffit: right
17:47:24 <RichiH> taffit: maybe folding in with -publicity could make sense; i never got why there are two teams
17:47:37 <lucas> but if, as a project, we don't manage to do something, there's not much we can do about it
17:47:38 <RichiH> or lucas could send a call for help
17:47:45 <lucas> yes, that could be tried
17:47:57 <lucas> #action lucas to include call for help for press team in next bits
17:48:06 <zack> RichiH: fwiw, one of the two is authorized (by the DPL) to answer press inquiries "signing" as Debian Project, the other team is not
17:48:20 <zack> (then we might debate whether that's needed/useful or not, of course)
17:48:43 <RichiH> zack: i think it's useful to have a trusted person who can speak for debian
17:48:59 <RichiH> but if that hinges on one single person with a backup of DPL, that's not ideal
17:49:12 <RichiH> part of that trust implies that they will not abuse that
17:49:21 <zack> right, so that's "we need a functional press team" argument, which I agree with
17:49:30 <lucas> we could make the DPL part of press@, as a last resort option, and use the press team as a group of people that (1) can advice on writing press releases; (2) help with the publishing, e.g. www.d.o
17:49:30 <zack> but it's unrelated to "we don't need a press/publicity" distinction
17:49:31 <bgupta> Do we get many press inquiries?
17:49:42 <RichiH> jftr, i will be split between here and #debconf15-germany in nine minutes
17:49:55 <RichiH> bgupta: i don't have hard numbers, but it does not appear to be that way
17:50:27 <zack> and there's another argument in addition to press inquiries, which are press releases; they are old style, but they are sort of needed in occasions like big sponsoring/agreement from company foo
17:50:34 <zack> because companies are old style in this respect
17:50:43 <zack> anyway, I'm off in a few minutes as well
17:50:45 <RichiH> lucas: leader@ can be on press@, but that increases your load so..
17:51:08 <lucas> yeah, but also motivation to offload this ;)
17:51:36 <taffit> What will happen in the mean time — e.g. who will take care of the upcoming point release announcements — is not settled AFAIK, maybe the RT should be made aware.
17:51:42 <bgupta> My sense is that one approach could be to fold the two teams together, allow DPL to ask new team to write responses press inquires, but have DPL retain final "signature" authority for those responses.
17:52:33 <lucas> I'll think about this and reply by email; I'd like to move one to the other topics
17:52:44 <lucas> #action lucas to follow-up on press team issues
17:52:49 <zack> bgupta: that might work, I agree
17:53:05 <lucas> #topic ** TODO RichiH to add Debian logins to http://www.debian.org/intro/organization.en.html
17:53:16 <RichiH> started locally, not finished
17:53:22 <lucas> #action RichiH to add Debian logins to http://www.debian.org/intro/organization.en.html
17:53:47 <lucas> #topic DONE stuff
17:53:57 <taffit> (please drop the end s/\.en\.html$/ of www.d.o URLs ;)
17:53:59 <lucas> I'm going to copy/paste, stop me if things need further discussion
17:54:14 <lucas> taffit: in that case it makes sense to have it
17:54:24 <taffit> no it doesn’t, really
17:54:29 <lucas> ** DONE rafw to investigate whether anti-harrassement should be added to www.d.o/contact
17:54:32 <lucas> ** DONE zack/paultag [binary-throw-away uploads] find relevant thread(s), write quick summary
17:54:33 <rafw> stop
17:54:35 <lucas> see http://deb.li/3NPsz
17:54:38 <lucas> ** DONE rafw to summarize status of debian-events on dpl-helpers@
17:54:40 <lucas> need input from lucas on future directions
17:55:10 <rafw> regarding anti-harassement, I need to put something sensible on the website still.
17:55:21 <lucas> ok, will action that
17:55:40 <lucas> #action rafw add something sensible about anti-harrassement on the website
17:55:51 <rafw> I will read the consitution and try to find info about the spirit.
17:56:01 <rafw> + regarding event team
17:56:02 <RichiH> lucas: i know two people who were on the anti-harassemnt team during dc13
17:56:08 <RichiH> maybe ask those for input?
17:56:11 <rafw> i did
17:56:14 <RichiH> k
17:56:35 <RichiH> (is that info public? i.e. who is on those aliases)
17:56:54 <rafw> regarding event team, the thing is it is more or less a one person team.
17:57:02 <lucas> yes, www.debian.org/intro/organization
17:57:21 <rafw> RichiH: nope, i wrote to antiharassement@d.o
17:57:23 <RichiH> yah, event@ seems very dead when you consider recent ml traffic
17:57:25 <rafw> ah ok
17:57:44 <rafw> yes, i spoke to Arne who is the main contributor to the team.
17:58:01 <rafw> He say to me he doesn't need help as somebody will start helping.
17:58:02 <rafw> but
17:58:23 <RichiH> rafw: rhalina will be around for the dc15 meeting, should i ask to join here?
17:58:29 <rafw> I don't thing that two people team is really a team.
17:58:35 <rafw> I talk to her as well.
17:58:43 <rafw> She offer to help later.
17:59:06 <rafw> I wrote to Lucas a summary in french.
17:59:21 <rafw> to be honest i am not sure what to do next.
17:59:36 <lucas> yes, I still need to think about it
17:59:54 <lucas> I've accumulated a small backlog of email during the last few days :(
18:00:03 <rafw> for me the problem is: if you want to have a list of next debian event ... atm you can't.
18:00:22 <rafw> I mean a gloval list up to date.
18:00:27 <rafw> global.
18:00:27 * zack gotta go, bbl
18:00:35 <rafw> zack: bye
18:00:39 <lucas> zack: bye, thanks for coming!
18:00:47 <bgupta> zack: later
18:00:54 <lucas> (I need to go quite soon too. max 10 mins)
18:01:09 <lucas> other remaining items are:
18:01:12 <lucas> ** DONE bgupta to explore legal issues around accepting cryptocurrency donations
18:01:15 <lucas> mail sent to SFLC, waiting for reply
18:01:18 <lucas> ** DONE lucas write call for helps to be included in next bits from DPL, for debbugs, dak, d-i
18:01:21 <lucas> done, except for d-i where nobody replied.
18:01:22 <lucas> ** DONE lucas check status of DC13 surplus
18:01:25 <lucas> now waiting for input from DC13 treasurer on what is possible
18:01:28 <lucas> ** DONE RichiH to review email about debian.* and send it to SFLC
18:01:30 <lucas> mail sent to SFLC, waiting for reply
18:01:54 <lucas> for d-i, it's not clear that it desserves a call for help, really. it would be nice if there was more activity, but it's far from dead, and not blocking atm
18:02:41 <lucas> actually, for DC13, the status is more "waiting for people to send requests for money"
18:02:41 <rafw> d-i : debian-installer ?
18:02:44 <lucas> yes
18:02:52 <rafw> ok
18:03:30 <rafw> regarding DC13: for info we get the money from the last sponsor a few day ago.
18:03:46 <bgupta> lucas: Quick note, I'd like to add an item that I will be exploring: Making a self hosted Debian donation page that largely allows people to donate directly from a webian page. (webian = www.d.o)
18:04:15 <lucas> rafw: was it sent to SPI or DC13?
18:04:26 <rafw> lucas: nope to DC13.
18:04:30 <RichiH> bgupta: making that easier and more seamless makes sense
18:04:35 <lucas> rafw: do you know the amount?
18:04:51 <rafw> yes, it is CHF 12'000 for NE.ch
18:05:11 <lucas> rafw: do you know if it's counted in the current surplus of CHF 38k, or in addition to that?
18:05:31 <rafw> lucas: I dont know
18:05:38 <lucas> ok, I'll ask hug
18:05:53 <lucas> bgupta: thanks a lot for working on that, that's indeed very important
18:05:56 <rafw> I guess it is inculded fur that is only a guess.
18:06:18 <lucas> #action lucas check whether NE.ch was already counted in the surplus
18:06:37 <lucas> #action bgupta investigate how to make a donations page on www.d.o
18:07:01 <lucas> bgupta: do you have something more to say on that, besides "it will be a challenge"? :)
18:07:40 <bgupta> I don't think it will be too bad.. will need to recruit a web dev though. (Gonna start without paypal support for now)
18:08:00 <lucas> ok
18:08:10 <lucas> anything else?
18:08:56 <lucas> ok, thanks a lot to you all for attending
18:08:59 <lucas> I'll post the minutes
18:09:02 <lucas> #endmeeting