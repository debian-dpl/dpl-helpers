#+TITLE: DPL helpers - working agenda
#+DATE:  [2013-10-09 Wed 17:00]

* roll call

* next meeting
$ date -ud @1382547600
Wed Oct 23 17:00:00 UTC 2013

* Current DPL TO-DO list

See "live" version at http://people.debian.org/~lucas/todo.txt
Q: anything missing?

* New topics

* Action items from last meeting (please update status and mention if discussion is needed. if not, we will just skip it during the meeting)

** TODO bgupta to follow up to TO email thread and cc dpl-helpers@
*** Done. Probably need to expand the conversation? Discussion needed.
** TODO bgupta confirm the final stored currency for debian.ch paypal account
*** Asked. Waiting for answer. No discussion needed.
** TODO RichiH to look into the status of the press team, and advise lucas on how to update the current delegation
** TODO RichiH to add Debian logins to http://www.debian.org/intro/organization.en.html

** DONE rafw to investigate whether anti-harrassement should be added to www.d.o/contact
** DONE zack/paultag [binary-throw-away uploads] find relevant thread(s), write quick summary

see http://deb.li/3NPsz

** DONE rafw to summarize status of debian-events on dpl-helpers@

need input from lucas on future directions

** DONE bgupta to explore legal issues around accepting cryptocurrency donations
mail sent to SFLC, waiting for reply
** DONE lucas write call for helps to be included in next bits from DPL, for debbugs, dak, d-i
done, except for d-i where nobody replied.
** DONE lucas check status of DC13 surplus
now waiting for input from DC13 treasurer on what is possible
** DONE RichiH to review email about debian.* and send it to SFLC
mail sent to SFLC, waiting for reply

