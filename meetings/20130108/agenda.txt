#+TITLE: DPL helpers
#+DATE:  [2013-01-08 Tie 18:00]

* happy new year everybody!

* next meeting
** date -d @1358877600 (January 22)

* review action items from last meeting
** TODO someone to check with bugs.d.o if a DPL-ish pseudopackage would be acceptable
** TODO paultag to do the README for the dpl magic ical script
** TODO zack to add a top-level TODO list, as opposed to agenda.txt
   which will be running "next meeting" agenda
** TODO lucas to wrap-up the salvaging/orphaning thread and submit dev-ref patch.
   - also address #681833
   - also look at the recent debian-qa@ thread.
** TODO Diziet to ask secretary@d.o about
   https://lists.debian.org/debian-project/2012/11/msg00009.html by next meeting
** TODO Diziet draft more formal statement re trademarks and discuss on -project
   apropos https://lists.debian.org/debian-project/2012/02/msg00073.html
** TODO zack to mail press@d.o an outline of the -companies announcement
** TODO algernon to update the DMCA draft directly with the identified changes
   rationale: for easier review
** DONE nhandler to add KGB bot to #debian-dpl
** TODO zack to contact debconf-team to draft a "job description" for future delegation

* highlights
** TODO status update of the release? (if RT people are around)
   - ETA for next bits?
   - ignore/wouldblock/etc tagging?
   - non-leaf removals?
   - anything we can help with?
** TODO new outbound trademark policy draft now posted
   - https://lists.debian.org/debian-project/2013/01/msg00024.html
** TODO authoritative list of DFSG-free licenses
   - https://lists.debian.org/debian-project/2013/01/msg00023.html
** TODO relationship with the FSF
   - they are still discussing FSF public stance internally
   - I've been invited to LibrePlanet 2013
     … to present (and discuss) further steps

# Local Variables:
# mode:org
# End:
