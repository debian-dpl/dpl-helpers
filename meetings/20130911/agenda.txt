#+TITLE: DPL helpers - working agenda
#+DATE:  [2013-09-11 Wed 17:00]

* Links:
** Bits from DPL for August: http://titanpad.com/dplbits0813
** DPL helpers BOF minutes: http://deb.li/3Wfhg

* next meeting
2013-09-24 Tue 17:00  (date -d @1380042000 ) ?

* Current DPL TO-DO list (copy/pasted from bits)
*** projects/ideas to help move forward / make sure it happens:
**** new contributors related
- Debian Welcome team (C: http://deb.li/ctX2 + BOF during DebConf -- can't
  find the notes anymore!  N: ??)
- how-can-i-help package (C: pkg in experimental.  N: upload to unstable,
  doc on wiki.d.o/how-can-i-help)
- packaging-tutorial package (N: upload new version, look at open bugs)
- "recruit" some example packages to point prospective contributors
  to. (N: send mail to -devel@, ping major teams)

**** other
- Debian Code of Conduct (C: http://deb.li/3wRWh  N: iterate with a
  new version?)
- binary-throw-away uploads -- related to reproducible builds (N: check
  what was the consensus, check status)
- debian-installer team (key team for swift releases) (C:
  http://deb.li/b0tF  N: write and send call for help?)
- package for dvd playing library (C: waiting for ftp-master review before
  final SFLC review  N: reping? do our own review first?)
- Debian PPA (would make transitions much easier to manage)
  (C: implementation not started yet AFAIK  N: ?)
- check status / aliveness of Debian events team (N: ping)
- debbugs (C: team with low manpower  N: check status, write call for
  help?)

*** delegations
- press team (C: someone stepped down.  N: update delegation)
- release team (C: some notes from DebConf meeting  N: draft delegation,
  iterate with team)

*** various other things
- dig mail archives to check status of Debian wrt OIN (Q from Simon Phipps
  at DebConf)
- enable donations via paypal (C: <20130505220527.GA4718@upsilon.cc>)
  + generally improve donations infrastructure
- write DPL part of DebConf13 final report. (C: http://deb.li/hHwh)
- check that anti-harrassement is mentioned on Debian website

* New topics

* Action items from last meeting (please update status and mention if discussion is needed. if not, we will just skip it during the meeting)
** Related to debian.* domains, probably covered by the current work inside the TM team to draft a policy
*** TODO lucas on debian.*, draft mail for -project, ask bgupta for review
*** TODO bgupta follow up with debian.mx
*** TODO bgupta make sure debian.rs transfer to SPI proceeds
** Other
*** TODO bgupta Help Martin (auditor) draft specs for TO requirements (Martin has agreed for help offer)
*** DONE bgupta/trademark@ continue logo registration process
*** TODO moray to propose a more detailed process about the teams survey
*** TODO moray to initiate work on paths into the project
