18:01:06 <zack> #startmeeting
18:01:06 <MeetBot> Meeting started Tue Nov 13 18:01:06 2012 UTC.  The chair is zack. Information about MeetBot at http://wiki.debian.org/MeetBot.
18:01:06 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
18:01:08 <Diziet> Hi.
18:01:15 <Maulkin> o/
18:01:18 <zack> who's here?
18:01:21 <lucas> hi
18:01:29 <moray> hello
18:01:39 <nhandler> Hi
18:01:43 <zack> algernon mentioned he'll probably be late
18:02:00 <zack> let's start with the easy ones
18:02:03 <zack> #topic next meeting
18:02:07 <zack> now + 2 weeks?
18:02:10 <lucas> ok
18:02:21 <moray> yes
18:02:22 <zack> eventually we'll probably need to slow down, but I think for now bi-weekly is probably still useful
18:02:50 <nhandler> For the next few meetings, I'll only be able to make the first 15 minutes or so, but don't change the time due to me
18:03:07 <lucas> why do you suggest slowing down?
18:03:23 <zack> lucas: I fear bothering people with excessive meetings, but if it's just me, even better :)
18:03:38 <zack> anyhow, now + 2 weeks it is
18:03:41 <lucas> well, as long as they are useful... :)
18:03:46 <zack> #agreed next meeting date -d@1354039200
18:04:02 <zack> nhandler: would you look into tech-ctte .ics thingie?
18:04:17 <nhandler> zack: Sure
18:04:37 <zack> #action nhandler to look into tech-ctte .ics thingie and replicate it to our case
18:04:51 <zack> #topic brief DPL report
18:05:03 <zack> nothing fancy on the report front, please have a look at the draft in agenda
18:05:10 <zack> if you've any questions, shout!
18:05:17 <paultag> ohh meeting.
18:05:53 <zack> ah yes, not mentioned there, but as you've seen Maulkin sent out the RT bits too, completing that item
18:06:15 <lucas> I guess there'll be a report about UDS on -derivatives?
18:06:20 <lucas> or there was one and I missed it?
18:06:31 <zack> lucas: no, you haven't, and you're right
18:06:46 <zack> in theory, stefanor should send it, by posting a dump of the etherpad used at the session
18:07:01 <zack> but anyone else could do it, as I don't plan to elaborate it any further
18:07:17 <zack> it just needs someone with launchpad account, willing to find out the titanpad entry and post it to -derivatives
18:07:30 <zack> I guess stefanor forgot or is not willing, anyone else to pick this up?
18:08:20 <lucas> it would probably be best if someone who attended UDS would post it:P
18:08:25 <moray> yes :(
18:08:40 <zack> uff, you're not helpful :-P
18:08:44 <nhandler> I have no issue posting it, but I can't add any additional info to it (I wasn't at UDS)
18:09:08 <zack> nhandler: please do! I'll take further questions on list if there are any
18:09:13 <zack> and you can mention that :)
18:09:30 <zack> nhandler: deal?
18:11:06 <zack> #action nhandler or zack: post UDS titanpd session for the debian/ubuntu healtcheck session
18:11:12 <zack> (to avoid forgetting)
18:11:20 <nhandler> zack: Yep, sounds good
18:11:44 <zack> #topic new participants
18:11:55 <zack> as you've seen, I announced this initiative more widely, pointing to this channel
18:12:08 <zack> as a result, we've new lurkers (and helpers, judging from nhandler volunteerism :))
18:12:33 <zack> it will probably induce some extra meta-discussions, like on the infrastructure part in the beginning
18:12:56 <zack> new participants: we have no command structure or anything, the people who were here before just volunteered early on!
18:13:15 * nhandler excuses himself and will read the minutes later
18:13:16 <zack> feel free to look through the agenda, and pick up stuff that interests you, or add some, mentioning it here
18:13:19 <zack> nhandler: bye!
18:13:26 * algernon waves
18:13:57 <zack> I guess there isn't much more to add on this, unless others have suggestions...
18:14:29 <zack> or, actually, we can move here the infrastructure part that seems relevant
18:14:37 <lucas> yup
18:14:56 <zack> we don't have an issue tracker, yet
18:15:05 <zack> and the titanpad agenda is, admittedly, rather messy
18:15:26 <paultag> So, what's the actual deal here, how does the process work?
18:15:34 <lucas> do we really need one? (issue tracker)
18:15:52 <zack> up to now, I've used my own work-flow to keep track of things
18:15:53 <paultag> helper processes task, results run through DPL, helper posts as the DPL's speaker?
18:16:05 <paultag> how can we be sure a DPL helper isn't going rouge? :)
18:16:11 <zack> but that sounds like a bottleneck, how else people could know with what they could help?
18:16:13 <paultag> do they speak with some authority?
18:16:16 <moray> lucas: perhaps, since some issues have apparently got lost -- on the other hand, trackers can create process that's unhelpful and discourage contributions
18:16:23 <zack> paultag: no, no special authority
18:16:24 <paultag> OK
18:16:32 <zack> just my thanks, for the moment :)
18:16:47 <zack> paultag: on the long term, I hope this will become officialized via some sort of board, possibly voted
18:16:48 <moray> (we just had someone quit a DebConf team due to its RT use...)
18:16:51 <paultag> zack: ACK
18:16:58 <lucas> paultag: what was agreed on is: you can state that you are doing $work as part of this initiative, but this does not give you superpowers.
18:16:58 <zack> but the idea is that first we need to convince (the project and ourselves) that this whole exercise is useful
18:17:05 <zack> otherwise is just useless extra bureaucracy
18:17:06 <paultag> so it's just a queue of stuff that needs a DD response, and if it's a "hot button" issue it actually requires DPL input?
18:17:23 <paultag> or non-DD
18:17:27 <zack> well, there are also actual initiatives
18:17:30 <moray> a list might be preferable above RT, until we see that a list doesn't work
18:17:33 <zack> that up to now have come only from my head
18:17:36 <zack> but that might come from others :)
18:17:51 <zack> moray: there's another point in favor of an issue tracker
18:18:00 <zack> passing over pending stuff from one DPL to the next
18:18:17 <lucas> wouldn't Ccing leader@ work with that too?
18:18:24 <paultag> BRB, will read minutes later
18:18:45 <zack> lucas: not really, when someone picks up, how do they know at a glance what's done and what's not?
18:18:56 <zack> that's in fact, what I had to do in 2010, and it hadn't been fun :)
18:19:09 <algernon> with regards to the tracker: if used well, it's a very powerful tool, it can make it MUCH easier to quickly glance at something, and know its status
18:19:15 <moray> zack: right, but after a few months there would be the same issue in a tracker tool :)
18:19:22 <lucas> yeah ok, but one would expect that there's a meeting between the old and the new dpl
18:19:27 <algernon> just care needs to be taken that it doesn't become an obstacle
18:19:52 <zack> lucas: there were, but one can disappear, etc.
18:20:03 <moray> zack: my opinion has actually switched a bit after seeing the rant from the person who resigned the DebConf team recently ;)
18:20:06 <zack> but I'm not sure I understand the reason of objections to the issue tracker idea
18:20:15 <zack> I mean, people could simply avoid using it if they don't want to
18:20:16 <Diziet> RT is pretty appalling.
18:20:27 <lucas> [moray: who was that?]
18:20:34 <Diziet> Constantly mangling email addresses and making the communication really hard.
18:20:44 <zack> oh, I see a fear
18:20:55 <Diziet> Also automatically turning every incoming mail into an issue is not always right.
18:20:56 <zack> I'm *not* saying that communication must go through $BTS
18:21:17 <Diziet> I think we could use bugs.d.o though.
18:21:20 <zack> I say that titanpad sucks as a device to keep track of what needs to be done :)
18:21:31 <moray> bugs.d.o would be a good solution, yes
18:21:41 <zack> and here's another thing
18:21:56 * lucas would agree with bugs.d.o too. I'm not an RT fan.
18:21:57 <zack> there *is* stuff that people expect the DPL to keep private
18:21:58 <moray> (as we all need to love bugs.d.o anyway)
18:22:01 <zack> they will ask for that
18:22:16 <zack> and while you can refuse to work on those stuff, that's a pretty lame answer if the need is real
18:22:23 <lucas> but then it shouldn't be forwarded to the DPL helpers anyway
18:22:29 <zack> correct
18:22:56 <zack> so, I'll need to have something private, and I don't see any better solution than RT
18:23:13 <zack> and once you've that, having another public queue to easily move stuff around would be nice
18:23:14 <Diziet> zack: If there are very few private issues they don't need tracking.
18:23:17 <lucas> what do you use currently? mail + TODO list?
18:23:29 * Diziet <- really hates RT
18:23:31 <zack> lucas: yes
18:24:04 <zack> everybody hates RT :)
18:24:17 <lucas> and you would move away from mail+TODO even without this DPL helper thing?
18:24:38 <zack> well, it's not moving away, it's rather looking for a complement
18:24:47 <zack> ideally that allows helpers to share an agenda and pick stuff up
18:25:34 <zack> in the end, I've set my mind on using RT for the private parts, at that point using something else for the public parts seems unwise to me
18:25:54 <zack> but again, if people refuses to use it, we can settle for bugs.d.o for the public parts
18:26:25 <lucas> RT can't be your agenda. you still need something more structured than a simple list
18:26:41 <lucas> with categories, etc.
18:26:44 <moray> zack: I realise there's a technical annoyance from having to switch stuff into bugs.d.o, or worse from realising that something is in the BTS that shouldn't be
18:26:55 <Diziet> I would rather have a post-it note on a wall in the locked cleaning cupboard in the basement behind a sign saying "beware of the leopard" than RT.
18:26:56 <moray> zack: but lots of people (not me especially!) do seem to hate RT
18:27:15 <zack> ok, I got the picture :)
18:27:31 <zack> so, next step is checking with BTS maintainers if they'd be fine in using a pseudopackage for DPL tasks
18:27:37 <moray> zack: I guess this is also a more general issue, for security bugs etc., so maybe it just needs some BTS work :)
18:27:46 <zack> moray: security team use RT :-P
18:27:57 <moray> zack: I know, at the mmentn
18:28:00 <moray> moment
18:28:01 <zack> (as DAM, DSA, etc.)
18:28:28 <zack> ok, it sounds like we're stuck in a typical "which software?" moment, I propose to postpone this
18:28:31 <zack> but
18:28:34 <moray> zack: (but hypothetically the BTS could be adapted to support this)
18:28:42 <lucas> (there are probably more urgent issues to fix with the BTS, such as moving to a more modern storage system ;) )
18:28:45 <zack> investigating whether a pseudopackage in the BTS would be acceptable is needed to decide
18:28:50 <Maulkin> .oO(This is taking quute a lot of time on this bit of the agenda...)
18:29:06 <zack> Maulkin: yes, I want to move on, but I'd like to have someone volunteer to check with BTS admins
18:29:31 <zack> as I wouldn't like "so let's do nothing" as a result :)
18:29:41 <zack> oh well
18:29:56 <zack> #agreed we need to check with bugs.d.o if a DPL-ish pseudopackage would be acceptable
18:29:57 <lucas> check whether it would be possible to create a dpl pseudo-package?
18:30:06 <zack> yes
18:30:11 <zack> they do not accept all pseudopackages...
18:30:42 <zack> let's move on
18:31:02 <zack> #topic new / urgent matters
18:31:17 <zack> Maulkin: thanks for the RT bits!
18:31:28 <zack> I've a question though
18:31:37 <zack> why do you plan to wait to start the ignore/block tagging?
18:32:33 <zack> Maulkin: ^^^
18:32:42 <lucas> (on a related note, I wrote the UDD bits to generate RC bugs stats (http://udd.debian.org/cgi-bin/rcblog.cgi), blogged it once, and Richard Hartmann picked up the task of sending them weekly.)
18:32:48 <lucas> s/why/when/? ;)
18:32:57 <zack> fwiw, my worries is that we enter an "after you ... after you" kind of thingie and delay the release for no good reason
18:33:13 <zack> lucas: no, it's why, I don't understand why waiting
18:33:52 <zack> looks like Maulkin is afk, let's move on...
18:33:53 <lucas> I see
18:33:54 <moray> my perception is that most developers see the release team doing a great job and are therefore just waiting for them to do the work and release :)   (perhaps not ideal)
18:34:10 <zack> moray: that's exactly my worry :)
18:34:19 <zack> and I'm struggling with imaging how to help...
18:34:34 <zack> the RC bugs posts have been great, and so are BSPs
18:34:50 <Maulkin> Sorry, phone call
18:35:01 <zack> but I don't see how else to encourage DDs to basically stop what else they're doing and focus on RCBugs
18:35:11 <lucas> the 'bad cop' way would be to state that we are aiming for a release in april/may
18:35:42 <moray> lucas: and then?
18:35:43 <zack> I was hoping jan/feb, but maybe that's good cop for you :)
18:36:12 <lucas> zack: look at the table on http://udd.debian.org/cgi-bin/rcblog.cgi :)
18:36:16 <Maulkin> zack: Basically, there's far too many RC bugs at the moment to run through the whole lot (or even start) - the priority is to drop them down. Last release we had about 200 left before we did mass tagging (IIRC). Mind you, we also had about 400 when we froze, rather than ~900
18:36:50 <zack> Maulkin: what scares me with that is, what if they don't drop as fast as you think? :)
18:37:07 <Maulkin> zack: Then we freeze for ~9 - 12 months
18:37:14 <zack> grmbl
18:37:17 <Maulkin> zack: That was my guess when we froze with 900 rc bugs
18:37:23 <moray> when I had a skim through things recently, I also had the feeling that some more packages just need to be culled altogether -- though I realise that isn't really a DPL matter
18:37:58 <Maulkin> zack: Thanks to certain people deciding to do toolchain uploads a few weeks before the freeze, despite knowing it months in advance.
18:38:22 <zack> moray: I can't remove "how to help the release team" from my high priority tasks, because the impact of that on project morale will be _huge_ one way or another
18:38:31 <moray> sure
18:38:56 <moray> but removing packages definitely doesn't depend on the release team, we could have some wider effort eaisly
18:38:57 <zack> so, as an encouragement to anyone here: please keep this in background, if you have smart ideas, please mention them here or to the release team directly
18:40:09 <zack> as time is growing short, let's move on to a related matter
18:40:16 <zack> you've probably seen the "no evil" thingie on -devel
18:40:27 <zack> in short, there are dozens of json related packages that are non free
18:40:48 <zack> the author has given exceptions to others in the past, allowing them to use the software "also from doing evil", making them free
18:41:05 <zack> I've received a proposal to ask the author for an exception
18:41:15 <zack> but it's not clear if that could be done without violating DFSG §8
18:41:28 <zack> (they've mailed me, because we'll probably need SFLC to word the exception)
18:41:38 <zack> DFSG §8 = "license not specific to Debian"
18:41:47 <moray> yeah, I wasn't sure what would work for that easily
18:41:49 <zack> it is my and Ganneff opinion that that's probably _not_ possible
18:42:16 <Diziet> If they are willing to grant a a broad enough exception then fine.
18:42:19 <moray> (although I wonder about the enforceability of a "use" clause anyway)
18:42:24 <Diziet> It would have to cover all of our downstreams.
18:42:39 <algernon> unless the exception is "anyone is also permitted to do evil" with it, it'd be against DFSG 8
18:42:48 <zack> Diziet: the question is, would writing "Debian and all recipients via Debian" be fine with DFSG?
18:42:57 <algernon> that's still debian specific
18:43:02 <Diziet> zack:
18:43:05 <Diziet> I think it would be fine.
18:43:18 <Diziet> Anyone who doesn't like that clause can get the copy off our ftp site.
18:43:23 <Diziet> It's like a dual licensing situation.
18:43:52 <Diziet> But this is not a decision we should be making here.
18:43:57 <zack> Diziet: right
18:44:15 <zack> Diziet: I've asked Ganneff (as ftp-master) and he disagreed, would you like if I get you in the loop
18:44:34 <zack> or any suggestions about where else this should be discussed?
18:44:41 <zack> (not sure if -legal would work, honestly...)
18:44:44 <Diziet> -devel or -project
18:44:49 <Maulkin> From a personal point of view, I would like to just see it all ripped out and replaced as it's actually a bit offensive of a licence. However, from a release PoV, I'd just like the bugs to go away
18:44:49 <Diziet> I think -legal is a swamp.
18:44:55 <Maulkin> -legal is a trap.
18:44:58 <lucas> maybe we could ask for a (transferable) licence do to evil with it, and dual license it?
18:45:09 <Diziet> lucas: Yeah, something like that.
18:45:22 <lucas> that would fit upstream's strange mind in a nice way :)
18:45:25 <zack> so, let's have this on -devel
18:45:39 <zack> I'll ping the person who proposed it, as I'll need permission to forward mail (or attribute it)
18:45:40 <algernon> or perhaps ask upstream to just dual license it? he could both keep his twisted license, and still not screw it up for the rest of the world
18:45:51 <zack> #action zack to raise the "no evil" proposal on -devel
18:45:59 <zack> algernon: already tried. didn't work
18:46:05 <algernon> ah
18:46:12 <zack> one that should be quick
18:46:25 <zack> anyone familiar with Debian mips porters these days?
18:46:45 <zack> I've got a proposal from a company who is interested in hiring 2 DDs full time to work on MIPS porting for small devices
18:46:50 <zack> I'll ask them to post to -jobs
18:47:03 <zack> but privately mentioning some people to contact for info would be nice
18:47:18 <zack> (and useful for the project, if things goes well)
18:47:23 <Diziet> zack: So is the question "is it OK for us to hire people" (yes) or "can we have a port" (surely via the normal criteria)
18:47:48 <zack> it's first of all understanding if the _current_ mips port is fine for their needs
18:47:55 <zack> that could probably be some consulting work
18:48:08 <zack> and then, if adaptations are needed, they'd like to contribute their work to Debian
18:48:09 <lucas> zack: there are 4 mips porters listed on http://release.debian.org/wheezy/arch_qualify.html, you could ask the release team who they are?
18:48:10 <Diziet> I don't think there is a difficulty with a port mostly done by a single company; that doesn't really threaten our independence or anything.  But we might ask them for a donation towards hosting if it was really mostly them.
18:48:16 <zack> via the person they'll identify
18:48:34 <zack> Diziet: agreed, in fact I think it'd qualify for Debian Partners or the like
18:48:51 <zack> lucas: good one, thanks!
18:49:06 <zack> Maulkin: mind /query-ing me with names of active mips porters?
18:49:58 <zack> moray, h01ger: for the debconf13 budget thing I guess we can postpone, right?
18:50:32 <Maulkin> zack: not sure where that number came from...
18:50:43 <zack> eh, I feared something like that :)
18:50:52 <h01ger> for the budget yes. but we should at least briefly discuss signing contract
18:50:54 <h01ger> :)
18:51:08 <zack> h01ger: you mean here/now? If so, fire, otherwise let's postponed that too
18:51:11 <moray> h01ger: budget should be agreed before contract signing
18:51:15 <zack> I'm still waiting for info about that too...
18:51:16 <algernon> zack: tbm used to be close to mips at some point, if I recall correctly. perhaps he could be asked for input aswell?
18:51:23 <lucas> Maulkin,zack: alternatively, #debian-buildd's topic says: mips*: p2-mate/ball, aba/*. contacting them might be a first step.
18:51:32 <moray> (given that the contract implies a fixed budget, and penalty clauses)
18:51:42 <h01ger> we have several budgets in svn ;)
18:51:45 <zack> lucas: thanks!
18:51:56 <Maulkin> lucas: ack :)
18:52:23 <zack> so, I'll wait for more info on that (debconf13) front
18:52:35 <zack> #topic pending actions
18:52:42 <zack> round of review of pending actions with done / re-action?
18:52:45 <h01ger> so i guess the next steps are sending current contract and some agreed budget to zack
18:53:07 <zack> h01ger: (ack)
18:53:11 <h01ger> zack, and then we shall use the miniconf to finalize things
18:53:28 <zack> back to pending actions
18:53:33 <lucas> #action lucas to wrap-up the salvaging/orphaning thread and submit dev-ref patch (not done yet). also address #681833. also look at the recent debian-qa@ thread.
18:53:43 <h01ger> (miniconf fr in paris that is)
18:53:46 <zack> it's all done up to Diziet -project discussion
18:53:53 <zack> on that front, I need to AOL Diziet's proposal
18:54:04 <zack> #action zack to AOL Diziet's DPL statements proposal
18:54:13 <zack> but I don't think it'll be consensual, to be honest
18:54:15 <zack> we'll see
18:54:20 <h01ger> on incoming sponsorship, we are at 53.5k CHF atm
18:54:33 <h01ger> which is "wh00t" IMO
18:54:38 <zack> Diziet: you still OK with trying to draft a formal statement for inbound trademark?
18:54:41 <Diziet> Yes.
18:54:48 <zack> great! please re-action that then
18:55:03 <zack> on the DMCA policy, I'm lagging
18:55:04 <h01ger> (and still a lot to go)
18:55:15 <Diziet> #action Diziet draft more formal statement re trademarks and discuss on -project, apropos of https://lists.debian.org/debian-project/2012/02/msg00073.html
18:55:18 <zack> algernon: it would help if you could update the draft document directly implementing the changes you identified
18:55:29 <zack> it will then be easier for me to go through your work and review it
18:55:32 <zack> algernon: sounds feasible?
18:56:00 <algernon> zack: yeah
18:56:11 <zack> algernon: great, please add an #action
18:56:18 <zack> #action zack to write an outline of the -companies announcement and mail it to press@d.o
18:56:21 <zack> (that's still pending)
18:56:45 <lucas> (salvaging still pending, already re-actionned)
18:56:54 <zack> Diziet: not sure if meeting organization is going better/worse now, but I could still use some tips on that front...
18:56:55 <algernon> #action algernon to update the DMCA draft directly with the identified changes, for easier review
18:58:00 <zack> oh, last one I forgot about the infra, more urgent than the issue tracker!
18:58:05 <zack> a mailing list
18:58:14 <zack> we've grown, it'd be nice to avoid private mailing now
18:58:33 <Diziet> Yes!
18:58:36 <zack> I could add a public mailing list to the Alioth group corresponding to the "dpl" unix group
18:58:45 <zack> would that work for everbody?
18:58:48 <lucas> yes
18:59:03 <h01ger> wouldnt a non alioth list be "better"?
18:59:11 <h01ger> as in lists.d.o
18:59:19 <lucas> it's for internal organization anyway, no?
18:59:21 <zack> h01ger: it's a big committment, and listmasters will want to be sure of traffic, subscribers, etc.
18:59:22 <algernon> as long as its a list, I don't think it matters much
18:59:33 <Diziet> The bikeshed should be located N-S not E-W
18:59:37 <zack> while the alioth list can be created with one click, by me
18:59:50 <h01ger> zack, right. and the dpl-team idea can grow slowly
18:59:51 <zack> Diziet: :-) but I do have actual arguments for alioth this time!
19:00:02 <zack> h01ger: or die :)
19:00:17 <h01ger> you said that :) but yes.
19:00:23 * algernon would go with alioth, for the sake of having an List-Id: header to filter mail on
19:00:26 <zack> ok, so alioth it is
19:00:40 <zack> #action zack to create a mailing list associated to the dpl "project" on alioth
19:00:53 <zack> for the bikeshed fans, I'll welcome list name suggestions after the meeting
19:01:08 <lucas> dpl-helpers@?
19:01:11 <lucas> ahhh, after. :-)
19:01:16 <zack> any other matters or can we close here?
19:01:27 <lucas> well the other infra git?
19:01:30 <lucas> err bit
19:01:34 <lucas> where to store agenda?
19:01:52 <zack> I can add other Git groups to that alioth project too, yes
19:02:01 <zack> would it be significantly better than titanpad?
19:02:08 <zack> (probably yes)
19:02:11 <h01ger> #action h01ger will push debconf-team to have enough information, agreed budget and a contract to sign by the end of miniconf in paris
19:02:16 <algernon> zack: yes
19:02:17 <moray> zack: in DebConf, we were reassured recently that the listmasters don't worry as much as they used to about overhead from new lists
19:02:31 <lucas> maybe we could use wiki pages under the dpl 'team', and titanpad for live editing during meetings?
19:03:11 <lucas> (wiki pages to store the history of agendas for each meeting)
19:03:23 <zack> so, to start with, I'll create a Git report in the alioth group
19:03:33 <zack> and move there the agenda from titanpad
19:03:38 <zack> which would be better anyhow, I guess
19:03:39 <moray> h01ger: formally I guess that should be "proposed" budget/contract, with DPL (and potentially Debian lawyers) to check/approve
19:03:58 <zack> for the wiki pages, no infrastructure tweaking is needed for that
19:04:09 <zack> #action zack to create a new git repo under the 'dpl' alioth group
19:04:14 <h01ger> moray, i'm not sure i want to go this road (approval by lawyers)
19:04:17 <lucas> I was talking about wikipages for agendas, instead of git repos
19:04:29 <zack> ah...
19:04:54 <zack> oh well, it's getting late, so let's postpone this
19:04:59 <h01ger> (and a budget is always a budget plan, so always a propose
19:05:04 <zack> I'll create the repo anyhow, what we put there is open to discussion
19:05:34 <lucas> ok, I have no strong opinion either way.
19:05:39 <zack> #endmeeting