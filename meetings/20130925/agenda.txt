#+TITLE: DPL helpers - working agenda
#+DATE:  [2013-09-25 Wed 17:00]

* roll call

* next meeting
$ date -ud @1381338000
Wed Oct  9 17:00:00 UTC 2013

* Current DPL TO-DO list

(Copy/pasted from http://people.debian.org/~lucas/todo.txt , which is updated more frequently)

*** projects/ideas to help move forward / make sure it happens:
**** new contributors related
- Debian Welcome team (C: http://deb.li/ctX2 + BOF during DebConf - http://deb.li/w3bM . recent email: http://deb.li/3tCUj )
- "recruit" some example packages to point prospective contributors to. (N: send mail to -devel@, ping major teams)
- adjust documentation around 'gift' tag. See http://deb.li/GWs4

**** other
- Debian Code of Conduct (C: http://deb.li/3wRWh  N: iterate with a new version?)
- binary-throw-away uploads -- related to reproducible builds (N: check what was the consensus, check status)
- debian-installer team (key team for swift releases) (C: http://deb.li/b0tF  N: write and send call for help?)
- package for dvd playing library. was reviewed by ftpmasters (N: Dmitry Smirnov to include feedback and provide a description of the packaging that can be reviewed by SFLC)
- Debian PPA (would make transitions much easier to manage) (C: implementation not started yet AFAIK  N: ?)
- [rafw] check status / aliveness of Debian events team (N: ping)
- debbugs (C: team with low manpower  N: check status, write call for help?)
- mariadb in Debian. See status in http://deb.li/ixEKc N: review+sponsor ...

*** delegations
- [richih] press team (C: someone stepped down.  N: update delegation)
- release team (C: some notes from DebConf meeting  N: draft delegation + see dpl-helpers.git:release-delegation.txt, iterate with team)

*** various other things
- dig mail archives to check status of Debian wrt OIN (Q from Simon Phipps at DebConf)
- [bgupta] look at donations using cryptocurrencies
- write DPL part of DebConf13 final report. (C: http://deb.li/hHwh)
- check that anti-harrassement is mentioned on Debian website

*** Someday/maybe (reservoir of ideas)
   continue to improve how-can-i-help (look at bugs)
   continue to improve packaging-tutorial (look at bugs)
   improve donations infrastructure (inc. donations via paypal, maybe)
   finalize inbound trademark policy - https://lists.debian.org/debian-project/2012/02/msg00073.html
   maintain authoritative list of DFSG-free licenses - RFH in http://deb.li/3zqEv
   patch UDD to mention odbl - http://deb.li/iOaes
   debian user survey
   debian local groups
   summarize "salvaging packages" thread as a dev-ref patch

   also see https://wiki.debian.org/Teams/DPL/Ideas

* New topics

* Action items from last meeting (please update status and mention if discussion is needed. if not, we will just skip it during the meeting)
** TODO RichiH to review email about debian.* and send it to SFLC
** TODO RichiH to look into the status of the press team, and advise lucas on how to update the current delegation
** TODO rafw to look into the status of the Debian events team, and advise lucas on possible course of action
** TODO bgupta to followup with auditor@ on 9/13 regarding proposed requirements for Debian TO
** TODO RichiH to add Debian logins to http://www.debian.org/intro/organization.en.html
** TODO bgupta to explore legal issues around accepting cryptocurrency donations
