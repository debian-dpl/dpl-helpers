18:00:09 <zack> #startmeeting
18:00:09 <MeetBot> Meeting started Tue Nov 27 18:00:09 2012 UTC.  The chair is zack. Information about MeetBot at http://wiki.debian.org/MeetBot.
18:00:09 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
18:00:15 <paultag> o/
18:00:18 <zack> hi everybody, who's around?
18:00:32 <zack> #chair zack
18:00:33 <MeetBot> Current chairs: zack
18:01:06 <nhandler> o/
18:01:15 <zack> while we wait for some more people to show up, an apology I've also sent on list
18:01:25 <moray> hello
18:01:36 <zack> at last minute (yesterday evening) I had to dive deep into a debconf13 matter
18:01:49 <zack> and I didn't have much time to work on the agenda, gladly nhandler picked up the routine part of it
18:01:59 <zack> #topic next meeting
18:02:09 * h01ger is around but doesnt intend to participate. i'm simply too exhausted
18:02:16 <zack> now + 2 weeks work for everyone?
18:02:25 <paultag> sounds good to me.
18:02:40 <nhandler> I'll have to leave early until around mid January, but that should be fine
18:02:51 <zack> can someone do the needed bash arithmetic expansion and paste here the magic 42 number?
18:03:02 <nhandler> zack: It is in the agenda
18:03:09 <zack> amazing
18:03:21 <zack> that would date -d @1355248800 then
18:03:46 <nhandler> Can someone (not me) test out the script in the git repo to update our ical?
18:03:52 <zack> #info next meeting date -d @1355248800
18:04:00 <zack> paultag: would you?
18:04:12 <paultag> sure, no problem.
18:04:49 <zack> in the meantime...
18:04:51 <zack> #topic (very) brief report of DPL activities
18:05:05 <zack> I've spent most of the time working on -cloud stuff
18:05:18 <zack> discussions on the -cloud list has been very intense and very effective
18:05:35 <zack> as a result, we now have official Debian images at http://deb.li/awsmp
18:05:44 <zack> (which expands to an awful long URL...)
18:06:05 <moray> great
18:06:05 <nhandler> Awesome work. I should give them a try.
18:06:12 <zack> I've delegated to James Bromberger to enter in agreement with AWS to be listed there (as he also did the bulk of the work)
18:06:27 <zack> and Amazon sponsored the account, so we're not paying for it
18:06:42 <zack> there's plenty of work to be done yet, which is slowly being tracked at http://bugs.debian.org/cloud.debian.org
18:06:55 <paultag> nhandler: btw, where's Date/ICal.pm live? I'll add it to a README
18:07:02 <paultag> nhandler: what do I have to apt-get :)
18:07:20 <zack> stuff has been quiet OTOH on the Azure front, they still need to tell us the bureaucratic stuff they would need
18:07:27 <zack> deadline is this friday to let us know
18:07:28 <nhandler> paultag: libdate-ical-perl
18:07:31 <paultag> thanks
18:07:48 <zack> I'vebeen postponing official communication (i.e. a press release) on this matter because I'd rather avoid giving the impression we're "endorsing" a single vendor
18:07:55 <zack> so having at least 2 would be preferred
18:08:14 <zack> then, the other big thing for the period is the debconf13 budget approval, but I'd rather leave that for the end of the meeting
18:08:35 <zack> if you've question on the above: fire! otherwise we can probably move to the pending action items
18:09:04 <zack> and maybe give paultag some time to breath :)
18:09:15 <paultag> bleh, I'm trying to find the module, in PM with nate
18:09:17 <paultag> will happen :)
18:09:36 <zack> oh good, testing is useful then ;)
18:09:45 <nhandler> Yep :)
18:09:54 <lucas> oops, forgot the meeting
18:09:56 <paultag> turns out the module is missing from the archive, will be laggy in updating it, but it's on the todo
18:10:03 <lucas> will read backlog now
18:10:08 <zack> so, let's single out infrastructure, as many action items were related to it
18:10:10 <paultag> in the meantime, one of y'all can run it off the cpan module or something.
18:10:13 <zack> lucas: hi!, no worries
18:10:28 <zack> #topic infrastructure
18:10:54 <zack> DONE nhandler to look into tech-ctte .ics thingie and replicate it to our case
18:11:04 <zack> first \o/, with paultag testing it
18:11:23 <paultag> :)
18:11:25 <Maulkin> o/
18:11:30 <Maulkin> (sorry for the delay)
18:11:31 <zack> DONE zack to create a mailing list associated to the dpl "project" on alioth
18:11:50 <zack> I've created the list and AFAICT all the active participants here have now subscribed
18:12:11 <zack> http://lists.alioth.debian.org/mailman/listinfo/dpl-helpers/
18:12:27 <zack> taffit: I've seen you mailed something there, but that has been before I subscribed myself (sorry :))
18:12:38 <zack> taffit: but frankly I didn't get the context...
18:13:12 <nhandler> I have to go. Feel free to give me any actions that you feel are appropriate. I will read the minutes later
18:13:19 <zack> nhandler: bye!
18:13:24 <zack> DONE zack to create a new git repo under the 'dpl' alioth group
18:13:32 <zack> I've mentioned this in my mail of today
18:13:47 <zack> if you haven't done so already, please request to join the alioth group
18:14:02 <zack> I'll accept the membership from anyone has been helping around here, and that should give the right to commit
18:14:02 <taffit> zack: was just fishing in case someone already tried or has interrest to include Debian CA SSL certificate into FireFox, Chromium, etc. browser
18:15:13 <zack> taffit: ok, although unfortunately I've no idea :-/
18:15:48 <zack> if you want, we can try to look into that, but we'll need someone to first look up the relevant contacts
18:16:29 <zack> to complete the infrastructure part, I haven't investigated the pseudopackage thingie, yet
18:16:42 <zack> but we can now move the agenda to the Git repo
18:16:52 <taffit> Disclaimer: no personal interrest/envy to dig myself into the CA SSL stuff
18:17:01 <zack> can someone volunteer to do so? (the agenda in Git)
18:17:10 <zack> I'd feel more safe with it in here than on titanpad...
18:17:46 <zack> in the meantime, let's remember about:
18:17:47 <zack> #action someone to check with bugs.d.o if a DPL-ish pseudopackage would be acceptable
18:19:01 <zack> #action paultag to test the addMeeting.pl script in the git repo
18:19:05 <zack> (it's happening anyhow)
18:19:06 <paultag> ack'd
18:19:19 <paultag> just ironing some issues with nate, will be done by EOD
18:19:29 <zack> #action someone to move agenda to new dpl-helpers git repository
18:19:38 <zack> paultag: sure, I don't think there's any hurry
18:19:46 <paultag> sure, I'll forget if I don't :)
18:20:07 <zack> anything else I've forgot about the infrastructure? (then we can move to the other outstanding actions)
18:20:30 <lucas> I don't think so
18:20:45 <zack> #topic pending actions
18:21:01 <zack> first one is mine
18:21:07 <zack> - zack to raise the "no evil" proposal on -devel
18:21:12 <zack> I think that's no longer needed
18:21:13 <paultag> erm, question
18:21:16 <paultag> what's going on with that?
18:21:34 <zack> so, as I understand it, the release team has declared wheezy-ignore
18:21:37 <zack> Maulkin: ^^^
18:21:44 <zack> meaning they're RC, but we ignore them for this cycle
18:22:05 <zack> I personally do not think that would need a GR, as the underlying non-free-ness looks questionable
18:22:18 <zack> paultag: does that answer your question?
18:22:29 <Maulkin> zack: Already set
18:22:39 <paultag> zack: I was wondering what you were going to propose to -devel
18:22:46 <paultag> but it's alright, we can do that later
18:23:19 <zack> paultag: ah, see the log of the last meeting. In short, someone proposed to word an exception, that we would have then asked upstream to grant to Debian
18:23:27 <paultag> ACK
18:23:32 <paultag> (thanks!)
18:23:37 <zack> the difficulties was in not violating DFSG in the point where licenses should not be specific to debian
18:23:44 <zack> but I think it's no longer needed now
18:24:05 <zack> and people on -devel seem fine with the release team take on it, so...
18:24:30 <moray> right, asking for an exception kind of encourages the guy, so if we don't need to do this, ...
18:24:40 <zack> indeed
18:25:01 <zack> lucas: next action (salvaging) is yours; question is "when"? :)
18:25:02 <moray> (if the release team believe that it can actually be fixed later)
18:25:19 <lucas> zack: end of year is crazy for me. ASAP, but unlikely this week
18:25:30 <zack> lucas: ack, please re-action
18:26:00 <zack> and yes, I know it's not a funny thing to do :), but it's the classical "important" stuff that the project will forgot if not concluded ASAP
18:26:09 <lucas> #action lucas to wrap-up the salvaging/orphaning thread and submit dev-ref patch (not done yet). also address #681833. also look at the recent debian-qa@ thread.
18:26:10 <zack> lucas: fwiw, you've my sympathies :-P
18:26:23 <zack> next is:
18:26:24 <zack> - zack to AOL Diziet's DPL statements proposal
18:26:29 <zack> I've done so
18:26:45 <zack> Diziet is not around, apparently, but then he has continued the discussion with plessy
18:27:16 <zack> relevant sub-thread is at https://lists.debian.org/debian-project/2012/11/msg00009.html
18:27:45 <zack> apparently, noone cares enough to complain, so we can probably go ahead (and out of private conversation I think Diziet agrees)
18:27:56 <zack> Maulkin: as secretary helper, do you see any blocker?
18:28:27 * Maulkin is looking...
18:28:38 <zack> in the meantime, the dependent action by Diziet is still pending, namely:
18:28:44 <zack> #action Diziet draft more formal statement re trademarks and discuss on -project, apropos of https://lists.debian.org/debian-project/2012/02/msg00073.html
18:29:06 <Maulkin> zack: you'll need q_ to rule on that. It's not straightforward
18:29:26 <zack> fair enough
18:29:29 <Maulkin> The question remains if you can delegate a task for which you are not empowered to perform yourself.
18:29:51 <zack> well, I'm convinced the answer to that is "no"
18:30:11 <Maulkin> I think it's clearly *intended* that you can, but if that matches the constitution, I'm not sure.
18:30:14 <moray> also depends what you mean by "empowered"
18:30:15 <zack> but the point is, any problem if we store somewhere documents where the DPL says "consensus on $list about $matter is $foo"?
18:30:18 <moray> i.e. white list or black list
18:30:49 <Maulkin> I'm also 100% sure it doesn't bloody matter in reality and any time spent on this issue is just navel gazing and would be much better off being spent on fixing RC bugs.
18:30:56 <zack> eh :)
18:31:03 <zack> Maulkin: you're in conflict of interest! ;)
18:31:27 <Maulkin> Or building a house at le camp so that there is > 0% chance I will attend.
18:31:31 <zack> Maulkin: any chance Q_ has _not_ followed the thread?
18:31:47 <Maulkin> zack: high chance, there's been nothing to secretary@
18:32:06 <zack> ok, so I'll add one for Diziet, at worst he'll complain and ask me to do that ;)
18:32:29 <zack> #action Diziet to ask secretary@d.o about https://lists.debian.org/debian-project/2012/11/msg00009.html
18:33:02 <zack> the next one is mine, and progress has been 0 on it ETOOBUSY:
18:33:03 <zack> #action zack to write an outline of the -companies announcement and mail it to press@d.o
18:33:28 <zack> algernon doesn't seem to be around either, but he mentioned postponing earlier on in the channel
18:33:37 <zack> #action algernon to update the DMCA draft directly with the identified changes, for easier review
18:35:04 <zack> the last one was for h01ger and it has now been done ("h01ger will push debconf-team to have enough information, agreed budget and a contract to sign")
18:35:07 <zack> #topic other business - debconf
18:35:26 <zack> I don't want to turn this into a yet another channel where discuss debconf13 matters
18:35:40 <zack> but budget approval is a pretty important step, and that's the first time we managed to follow the process
18:35:59 <zack> moray, h01ger: anything you want to mention about that, which would be relevant in this context?
18:36:08 <zack> (same for anyone else, of course)
18:36:08 <moray> zack: approval should probably also mention if/that re-approval is needed for "material changes"
18:36:17 * h01ger thanks zack for his support in getting this process set up and followed through for the first time too
18:36:24 <moray> I guess it should be needed, otherwise the approval step is meaningless :)
18:36:35 <h01ger> and iirc this process is not documented in the wiki manual we have
18:36:53 <zack> fwiw, my approval mail is here http://lists.debconf.org/lurker/message/20121127.150018.7cf4a1be.en.html
18:36:56 <zack> (damn lurker)
18:37:09 <zack> h01ger: it should be, I agree
18:37:25 <zack> h01ger: fwiw, I've now documented the -chairs@d.o role address at www.d.o/intro/orga now
18:37:26 <h01ger> moray, depends what is approved
18:37:33 <h01ger> zack, awesome, thanks!
18:37:52 <zack> h01ger: do you have a place where to note down that the approval process is still to be documented?
18:37:59 <zack> moray: I agree, so here is my take on it
18:38:10 <paultag> just as an outsider
18:38:12 <zack> - in the approval, I've added some constraints, and I expect them to be followed
18:38:27 <paultag> I'd be really nice to get this cleaned up, folks (such as myself) are waiting to book tickets to come out
18:38:32 <zack> - I then expect to be asked for approvals for changes in the overall figures that would increase the deficit
18:38:33 <paultag> so the longer this goes on, the worse it's going to hurt :)
18:39:08 <zack> and of course I think the notion of "changes" should be flexible, i.e. don't bother if it's <5%
18:39:11 <zack> moray: does that make sense?
18:39:19 <h01ger> zack, bugs.d.o/dpl-helpers? ;) /me scratches head. (its in my todo, and rt.debconf.org...) so, dpl helpers meeting agenda?
18:39:40 <zack> h01ger: I was rather wondering if you didn't have TODO holes in the debconf manual
18:39:49 <zack> that sounds like a more sensible place to me
18:39:52 <h01ger> right
18:40:07 * h01ger creates that now
18:40:25 <moray> zack: right.  the "material changes" phrase is a jargon thing, it's not just about >5% money changes, but supposing we decided to spend the same amount of money on champagne instead
18:40:27 <zack> h01ger: you can point to the mail i've mentioned above as a reference (it's also the only one we have ;-))
18:40:42 * zack didn't know the term
18:40:44 <moray> zack: I think the intention is clear from your email, but I've thought a lot of things that it turned out others didn't agree with :)
18:41:03 <zack> but yes, I definitely agree that switching to champagne is not something I'd consider allowed :)
18:41:20 <moray> so it could be worth re-stating that the caveats are also meant to be followed (or asked about), not just suggestions
18:41:22 * h01ger is not sure if thats a wise restriction but atm i really dont care
18:41:29 <zack> otoh, if/when new sponsors pop up, I'd rather be happy if the debconf team can put the money into good use without having to ask me at each change
18:41:47 <zack> is there any sane minded way of defining that so that it doesn't become obnoxious for everyone?
18:42:15 <moray> well, I think as long as there's some "material changes" kind of phrase it makes what is *meant* to happen clear, and people can ask if they're not sure
18:42:35 <zack> do you have a reference for that expression?
18:42:45 * Maulkin can find the standard wording from UK governmnet procurement contracts if you want.
18:43:06 <Maulkin> It's usually (>= 5%, debconf chairs need to approve. >= 10%, DPL needs to approve)
18:43:15 <Maulkin> Or similar
18:43:36 <zack> uhm, how does account for the debconf vs champagne switch? :)
18:43:46 <zack> s/does/does that/
18:44:01 <Maulkin> Define the purpose for each line in the budget.
18:44:24 <zack> moray: my bottom line is: I'd be happy to clarify that, but I also don't want to give the impression that I want to question individual "reasonable" changes by the team
18:44:33 * h01ger thinks we should consider the champagne switch.
18:44:37 <zack> if you find a wording to make that clear, I'll be happy to follow it
18:45:00 <zack> also, this is the first year we manage to have the step in the firstplace, so I wouldn't mind if we refine things and become more precise in future editions
18:45:07 <moray> sure
18:45:26 <moray> I don't think it's needed/wise to precisely define numbers or have lots of legalese about it
18:45:36 <zack> moray: would you keep this in the backburner and let me know if you come up with a decent explanation for -team?
18:45:47 <moray> I just don't want anyone to think "ok, dpl approval done" then completely ignore changes
18:45:58 <Maulkin> zack: For each line provided in the budget (accomodation, flights, infrastructure, catering), debconf chair approval must be received in gpg signed mail to exceed the budgeted amount by 5%. DPL approval by 10%.
18:46:17 <zack> moray: yes, and if you or anyone else notice anything of the sort, please point me to it
18:46:26 <zack> it would be a change to do an explanation by example :)
18:46:29 <zack> chance
18:46:34 <Maulkin> If people then try and mess with it, I don't think *any* rules we make will stop that.
18:46:56 <moray> zack: ok.  but perhaps at least explain that the points in your email aren't merely suggestions, but part of the approval
18:47:00 <zack> right, that's why I care more about explaining the principle, than establishing rules
18:47:13 <h01ger> http://wiki.debconf.org/wiki/TODO - there
18:47:50 <zack> moray: ok, can you please ping me about this tomorrow?
18:48:05 <zack> h01ger: thanks!
18:48:15 <moray> zack: sure
18:48:31 <zack> moray: great
18:48:45 <zack> paultag: I didn't forget your worry
18:48:58 <paultag> NP, bigger things going on.
18:49:12 <zack> fwiw, we're years ahead of the usual notification of "book your tickets" :)
18:49:16 <paultag> :)
18:49:21 <zack> which is not a good sign about past conference editions
18:49:30 <zack> but also means there is still margin to do better ;)
18:49:56 <zack> anything else about the budget approval?
18:50:15 <Maulkin> zack: Hrm
18:50:36 <Maulkin> zack: If I was doing approval, I would be very careful to check assumptions about numbers attending.
18:50:49 <Maulkin> zack: Apart from that, not really
18:51:04 * Maulkin is happy to look too if you want.
18:51:05 <zack> so, I did quite some checking this morning, with hug
18:51:24 <zack> the low-attendance scenarios where in fact better, from a budget perspective
18:51:29 <zack> (worse for debconf, of course)
18:51:49 <h01ger> it all depends on the sponsorship money raised
18:52:00 <moray> Maulkin: fortunately we seem to have moved beyond the initial ridiculously optimistic versions.  though I note darst still queried some items
18:52:07 <lucas> what about doing a quick poll to determine who would not come to debconf at LeCamp ?
18:52:21 <zack> lucas: sorry, but that doesn't belong here, I think
18:52:28 <Maulkin> moray: So, eg: reliance on professional attendance. Reliance on people to be able to pay their own travel etc...
18:52:41 <lucas> zack: I did not mean here. doodle-like, with all potential attendees
18:52:58 <zack> lucas: what I meant is that I don't want to "steal" debconf organization to this channel
18:53:09 <moray> Maulkin: at the moment the base case seems to be that we raise a lot of sponsor income and can just about pay for one week of it empty.  so as long as we are careful, we can also find other combinations beyond that
18:53:12 <zack> if you think the proposal has merits, please advance that to -team, or equivalent
18:53:17 <lucas> ok
18:53:23 <h01ger> lucas, so put this on planet and lpn? publically questioning a decision taken 3 times already?
18:53:26 <zack> I just wanted to discuss here the dpl relevant part, i.e. budget
18:53:28 <h01ger> lwn
18:53:31 <moray> Maulkin: I would appreciate continued vigilence that overly optimistic things don't sneak back in more
18:53:35 <zack> h01ger: please
18:53:50 <Maulkin> moray: I would if there was a sane interface to lists.debconf.org
18:53:59 <Maulkin> moray: especially as I don't subscribe to -team
18:54:02 <h01ger> zack, that was what lucas was effectivly proposing..
18:54:17 <zack> h01ger: yes, and given he agreed to take it elsewhere, I think it'd be fair if you do it too
18:54:24 <h01ger> s/lwn/d-d-a/ if you like but i think if we do debconf for debian newbees too...
18:54:44 <lucas> I'm proposing getting accurate data about number of attendees
18:54:49 <lucas> that's all
18:55:12 <lucas> anyway, let's move on
18:55:12 <zack> in other news, we're 5 minutes short of the end of the meeting
18:55:15 <h01ger> 
18:55:18 <zack> #topic misc
18:55:27 <zack> anything else about anything else?
18:55:30 * nhandler got back early and will handle the action about moving the agenda to git
18:55:37 <lucas> re. cloud & amazon stuff, I haven't been able to renew our Debian QA credits on AWS yet (which are expiring at the end of the year). i've been talking to James Bromberger about it. If it does not work out, I could use contacts at other public cloud providers.
18:55:54 <zack> lucas: ah, great, thanks for looking into that
18:56:05 <zack> out of curiosity, how much did we use of the donated amount in 1 year?
18:56:27 <lucas> 2k out of 10k, because we started late and used spot instances, which are much cheaper
18:57:01 <zack> so a reasonable estimate is what, 5k/year?
18:57:13 <lucas> something like that
18:57:23 <zack> cool, thanks
18:57:35 <zack> anything else?
18:58:16 <zack> ok then...
18:58:27 <zack> #endmeeting