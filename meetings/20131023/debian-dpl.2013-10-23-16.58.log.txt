16:58:08 <lucas> #startmeeting
16:58:08 <MeetBot> Meeting started Wed Oct 23 16:58:08 2013 UTC.  The chair is lucas. Information about MeetBot at http://wiki.debian.org/MeetBot.
16:58:08 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
16:58:11 <lucas> hi!
16:58:24 <lucas> agenda is at http://titanpad.com/debiandpl-20131023
16:58:33 <lucas> #topic roll call
16:59:02 <lucas> MeetBot: pingall dpl helpers meeting now!
16:59:02 <MeetBot> dpl helpers meeting now!
16:59:02 <MeetBot> algernon anoe bdrung bgupta buxy chattr Diziet Ganneff gnugr gregoa highvoltage hug joehh KGB-0 KGB-1 KGB-2 laarmen lindi-_ luca lucas marga MeetBot moray nhandler pabs paultag rafw taffit taffit_sud zack
16:59:02 <MeetBot> dpl helpers meeting now!
17:00:20 <lucas> am I on my own? :)
17:00:25 <rafw> hello
17:00:30 <rafw> i am here :)
17:00:35 <bgupta> here
17:00:53 <lucas> cool, thanks. had a fear for a few seconds.
17:00:58 <bgupta> I know zack sent regrets.
17:01:03 <lucas> yes
17:01:19 <lucas> agenda is that http://titanpad.com/debiandpl-20131023
17:01:25 <lucas> #topic next meeting
17:01:31 <lucas> Lucas is travelling quite a lot during november
17:01:32 <lucas> #action lucas send a doodle poll with possible dates
17:01:35 <lucas> (copy pasting)
17:02:09 <lucas> I can't make it to the usual times, so we will need to shuffle things a bit, unless you think it's better to keep the same time and have a meeting without me
17:02:31 <rafw> lucas: i don't think we shoudl have meeting without you.
17:03:06 <bgupta> lucas: agree with rafw
17:03:13 <lucas> ok, then I'll do a doodle poll with the few dates where I'm actually available in november
17:03:32 <lucas> #topic * Current DPL TO-DO list
17:03:40 <lucas> See "live" version at http://people.debian.org/~lucas/todo.txt
17:03:41 <lucas> Q: anything missing?
17:03:51 <lucas> I'll update it after the meeting with today's topics, of course
17:04:31 <lucas> my =dpl inbox was at 0 emails on monday morning, but is now back at 15
17:04:45 <bgupta> (Sorry I think a number of those were me)
17:05:06 <lucas> heh only 6 ;)
17:05:58 <lucas> and my private todo list has one item, which not urgent/not so important
17:06:34 <lucas> #topic * New topics
17:06:39 <lucas> anything to discuss?
17:07:19 <bgupta> there was an item check with SFLC about bitcoin donations, it's waiting on their reply.. It seems it was removed from agenda?
17:07:50 <bgupta> we can discuss later.
17:07:54 <lucas> err, was it?
17:07:56 <highvoltage> o/
17:08:17 <lucas> I might have made a mistake reorganizing the agenda to group items that might not require discussion
17:09:16 <bgupta> no worries.
17:09:37 <lucas> oh, one thing in the "New topics" is that I'll be looking for a co-maintainer for the how-can-i-help and packaging-tutorial packages. i've had trouble keeping up with the load
17:10:04 <lucas> I'll probably advertise this in my next bits
17:10:17 <lucas> #topic * Action items from last meeting
17:10:35 <lucas> I'm going to copy/paste the ones which don't seem to require discussion. let me know if I'm wrong, of course.
17:11:04 <lucas> ** DONE zack find status of debbugs submissions over HTTP
17:11:05 <lucas> C: http://deb.li/3F03i N: follow-up on #590269
17:11:05 <lucas> ** DONE lucas to follow-up on press team issues
17:11:05 <lucas> awaiting feedback, still on the radar
17:11:07 <lucas> ** TODO rafw add something sensible about anti-harrassement on the website (C: draft almost ready N:once draft is finished, fill a bug similar to #719467)
17:11:26 <lucas> that last one needs re-action for tracking purposes, I guess?
17:11:40 <rafw> yes, now that I know what to write on the website.
17:11:47 <lucas> #action rafw add something sensible about anti-harrassement on the website (C: draft almost ready  N:once draft is finished, fill a bug similar to #719467)
17:11:48 <rafw> I just need to fill the bug.
17:11:51 <lucas> ok, thanks
17:11:56 <rafw> welcome
17:12:19 <lucas> ** TODO zack to draft questions for a survey of DDs
17:12:19 <lucas> *** no progress, re-action
17:12:19 <lucas> #action zack to draft questions for a survey of DDs
17:12:19 <lucas> ** TODO bgupta improve https://wiki.debian.org/Teams/Auditor/Organizations based on TO definition discussions
17:12:22 <lucas> *** No progress. re-action
17:12:24 <lucas> #action bgupta  improve https://wiki.debian.org/Teams/Auditor/Organizations based on TO definition discussions
17:12:27 <lucas> ** TODO bgupta respond to TO draft thread
17:12:30 <lucas> *** Awaiting a promised meeting with Martin (auditor)
17:12:33 <lucas> *** reaction
17:12:35 <lucas> #action bgupta respond to TO draft thread (after meeting with tbm)
17:12:40 <lucas> #action bgupta respond to TO draft thread (after meeting with tbm)
17:12:40 <lucas> ** DONE lucas check whether NE.ch was already counted in the surplus
17:12:40 <lucas> the NE.ch donation was already counted in the announced surplus.
17:12:41 <lucas> general status on this: call for ideas sent, many ideas thrown in the discussion, not so many resulted in actual actionable expenses
17:13:09 <lucas> ** TODO RichiH to add Debian logins to http://www.debian.org/intro/organization
17:13:12 <lucas> *** no progress, re-action
17:13:15 <lucas> #action RichiH to add Debian logins to http://www.debian.org/intro/organization
17:14:13 <lucas> ok, the next ones are more likely to welcome discussion. bgupta: do you want to start with the bitcoin one, or the general donations one?
17:14:51 <bgupta> They tie together.. the bitcoin is in my mind just another payment processor. donation method, that is lower on my prioity list than paypal
17:15:10 <bgupta> but once we have paypal support I want to make that the next priority
17:15:17 <lucas> #topic donations: paypal & bitcoin
17:15:30 <lucas> content of the agenda is:
17:15:33 <lucas> ** TODO bgupta investigate how to make a donations page on www.d.o
17:15:33 <lucas> *** Done: new subtasks:
17:15:33 <lucas> **** bgupta: Work with SPI to get an usaepay API key
17:15:33 <lucas> **** bgupta: Work with SPI to get a Debian controlled Paypal account
17:15:33 <lucas> ***** SPI should be voting on proposal next board meeting: date -d @1384459200"
17:15:36 <lucas> **** bgupta: Write an HTML mockup for new donations page, and attach mockup to bug #681501
17:16:09 <bgupta> we should attend next SPI board meeting as advocates.
17:16:48 <bgupta> they will be voting on my proposal to allow debian to have a seperate bank account and our own Paypal accoount that we would be allowed to administer.
17:17:21 <bgupta> it looks promising but I was told it can't hurt if we have a few advocates at the meeting in case there are questions/concerns
17:17:28 <rafw> why a separate bank account?
17:17:35 <lucas> ok, I should be able to make it. will you?
17:18:14 <bgupta> I plan to… it's a little far out for me to know my schedule with 100% certainty, but I'm not travelign or anything, and will try my best to keep it free
17:18:32 <bgupta> rafw: I'll loop you in after meeting
17:18:55 <bgupta> (long strory)
17:18:58 <rafw> bgupta: I will get some food after the meeting.
17:19:03 <rafw> bgupta: ok thanks.
17:19:13 <bgupta> I'll send an email then.
17:19:19 <lucas> for bitcoin, what's the status?
17:19:25 <lucas> well, cryptocurrencies in general
17:19:42 <bgupta> still waiting on Mishi's response, but I told her it was low priority..
17:20:15 <bgupta> yeah I think the query was a general cryptocurrency inquiry.
17:20:48 <lucas> mmh, on my "pending SFLC questions", I have the "domain names & trademark" one
17:21:00 <lucas> I think that's wrong, because the ball is on our side, right?
17:21:09 <bgupta> Yes..
17:22:17 <lucas> how do you feel about this? in my todo list, it's "assigned" to trademark@, but it's not really a trademark issue anymore, right?
17:22:56 <rafw> I don't really follow on that issue.
17:23:01 <bgupta> I'd agree. I still strongly feel that allowing non-debian people/entities to use debian.* domains is not the right thing todo, whatever trademark feels. It's a project issue now.
17:23:13 <bgupta> (Except TOs of course)
17:23:46 <lucas> rafw: sorry, the discussion has been happening inside the trademark team, whose archives are not public
17:24:04 <rafw> lucas: sure, np.
17:24:35 <lucas> rafw: the general question is what we should do about debian.* domain names
17:25:24 <rafw> I have heard we will have a lot more of tld soon.
17:25:32 <bgupta> lucas I can take a stab at writing up a proposal for you to tweak and review.. It can deal with grandfathered domains, and I'll reference the Fedora domain policy which it turns out, is very similar to the approach I think we should be taking.
17:26:12 <bgupta> (Thanks to joehh for finding the fedora policy)
17:26:14 <lucas> bgupta: that would be great
17:26:31 <bgupta> feel free to action that to me.
17:26:57 <lucas> #action bgupta write a proposal for handling of debian.* domain names
17:27:22 <lucas> rafw: looking at my todo list, my email about the events team had a question for you
17:27:43 <lucas> rafw: have you had time to take a look at it? (email sent on monday)
17:27:48 <rafw> yes, i didn't reply so far.
17:28:19 <rafw> I mostly agree with you.
17:28:38 <rafw> Do you want me to continue or do you  prefer to carry on ?
17:28:57 <rafw> For me it doesn't matter.
17:29:19 <lucas> I'm all for offloading, but it might become uncomfortable for you at some point.
17:29:42 <lucas> maybe try to do another round of emails, and we will see?
17:30:10 <rafw> Yes, I will try to contact the team and see what happen. Shall I copy you ?
17:30:33 <lucas> rafw: yes please
17:30:59 <lucas> rafw: note that leader@ is archived, and is quite useful to dig into past issues, so generally it's better to write to leader in english
17:31:20 <rafw> yes, i notice.
17:31:29 <lucas> even if I agree that french would be so much easier, I don't think the Debian project is ready yet to switch to french as the official language
17:31:37 <rafw> :)
17:31:54 <lucas> maybe another french-speaking DPL and we can start pushing for it ... :)
17:32:10 <bgupta> no, we should switch to esperanto!
17:32:19 <bgupta> <jk>
17:32:38 <lucas> heh
17:32:39 <lucas> :)
17:32:42 <taffit> www.d.o already has more French pages than English ones…
17:32:59 <lucas> #action anything else?
17:33:01 <lucas> oops
17:33:06 <lucas> #topic anything else?
17:33:28 <bgupta> We never actioned the new subtaks
17:33:33 <rafw> not on my side.
17:33:49 <bgupta> for the donations stuff
17:33:54 <bgupta> I can do so.
17:33:55 <lucas> **** bgupta: Work with SPI to get an usaepay API key
17:34:02 <lucas> is this a typo? usaepay?
17:34:14 <bgupta> it's either usepay or usaepay.
17:34:15 <lucas> ah no
17:34:26 <taffit> https://www.usaepay.com/
17:34:33 <lucas> err, please action :)
17:34:48 <bgupta> SPI uses it, and unlick clickandpledge we can use it on our own pages.
17:35:01 <bgupta> #action bgupta: Work with SPI to get an usaepay API key
17:35:16 <bgupta> #action bgupta: Work with SPI to get a Debian controlled Paypal account
17:35:32 <lucas> what will be the technical issues with the donations page improvement?
17:35:36 <lucas> e.g. javascript?
17:35:47 <bgupta> #action lucas: bgupta: SPI should be voting on proposal next board meeting: date -d @1384459200
17:35:56 <bgupta> no technical issues, should just be doable.
17:36:17 <bgupta> (If my understanding is correct.. we're not ooking to do progress bars or anything, just make it easier to donate)
17:36:39 <bgupta> technical issues are *I* need to learn enough HTML to write the mockup.
17:36:44 <lucas> ok :)
17:37:03 <bgupta> #action bgupta: Write an HTML mockup for new donations page, and attach mockup to bug #681501
17:37:20 <lucas> ok, anything else?
17:37:24 <bgupta> my proposal to SPI may warrant further discussion
17:37:36 <bgupta> but it's not pressing and could wait for another meeting
17:38:12 <lucas> the paypal one? so far I fully agreed with what you said in the mail thread
17:38:39 <bgupta> Basically I proposed that Debian has it's own paypal account that debian delegates could manage. Those debian delegates would need to be SPI Contributing Members, and go through a vetting making sure they understood the basics of us non-profit law.
17:39:04 <bgupta> All DDs qualify as SPI CMs, so it would be a formality.
17:39:23 <lucas> ideally, there could be some overlap between auditor@ members and those delegates
17:39:34 <bgupta> sure..
17:40:03 <bgupta> I kinda had the same thought..
17:40:26 <lucas> or we could make this part of the auditor@ delegation, but that makes it a bit trickier to include non-US people
17:40:37 <bgupta> why?
17:40:46 <lucas> as they might not meet the "understand the basics of us non-profit law" requirement :)
17:40:56 <bgupta> No they can be taught..
17:41:03 <bgupta> they don't have to know them first.
17:41:14 <bgupta> I can write it up in like 20 mins
17:41:24 <lucas> ok :)
17:41:35 <lucas> not knowing about it, I was expecting worse
17:42:31 <lucas> I'd rather have this part of the auditor@ delegation. splitting roles further makes things worse from a team-status-tracking POV
17:42:32 <bgupta> it's largely how you are allowed to spend money… (there is also a rule against using NP resources for politics, but I don't think that would be relevant for us).
17:43:01 <lucas> how do they define politics?
17:43:11 <lucas> e.g. does the FSF still qualify?
17:43:12 <bgupta> US Government politics
17:43:15 <lucas> ok
17:43:24 <bgupta> like lobbying..
17:43:39 <bgupta> telling people who to vote for..
17:43:59 <lucas> ok
17:44:24 <bgupta> I'll cover it when I write up a briefing.. perhaps we can action that to me now..
17:44:39 <lucas> might be a bit early, no?
17:44:49 <bgupta> or we can wait and make sure the PSI does in fact vote.. Yeah. It's early..
17:45:05 <lucas> ok, let's wait for now
17:45:32 <bgupta> oh I should action the cryptocurrency thing
17:46:08 <bgupta> #action bgupta: Investigate accepting crypto-currency donations (With SFLC help)
17:46:19 <bgupta> I'm done. L)
17:46:21 <lucas> ok, I think that ends the meeting. of course, feel free not to wait for the next meeting to raise up things
17:46:22 <bgupta> :)
17:46:36 <rafw> sure
17:46:37 <lucas> #endmeeting