===================
#debian-dpl Meeting
===================


Meeting started by zack at 17:01:45 UTC. The full logs are available at
http://meetbot.debian.net/debian-dpl/2012/debian-dpl.2012-10-23-17.01.log.html
.



Meeting summary
---------------
* agenda / 2 weeks status report  (zack, 17:03:44)
  * volunteer needed to request creation of debian-cloud@lists.d.o and
    see how it goes  (zack, 17:06:05)
  * ACTION: topic to raise at UDS: ask what Debian could do to avoid
    Ubuntu's need to fork packages  (zack, 17:14:42)

* important/urgent topics  (zack, 17:18:09)
  * ACTION: zack (at UDS) to ask Ubuntu contributors to send a mail to
    inform relevant Debian maintainers when a UDS proposal affect their
    packages, so they know about it/ can provide feedback before
    implementation starts  (lucas, 17:18:55)
  * suggestion for release process: UDD-based script to generate
    Tolimar-like bugs stats, and give evidence of BSP results and the
    like  (zack, 17:26:45)
  * suggestion for the release process: mechanism to encourage "peer
    review" of unblock requests, RC bug triage, etc  (zack, 17:27:14)
  * ACTION: zack to draft a RT mail outline and pass it on to RT  (zack,
    17:29:31)
  * patches welcome to improve niels workflow for identifying removals
    svn://svn.debian.org/svn/collab-qa/rc-buggy-leaf-packages  (zack,
    17:33:28)

* review action items from last meeting  (zack, 17:35:27)
  * ACTION: Diziet start conversation on -project about dpl statement
    process  (Diziet, 17:37:01)
  * ACTION: Diziet draft more formal statement re trademarks and discuss
    on -project, apropos of
    https://lists.debian.org/debian-project/2012/02/msg00073.html
    (Diziet, 17:37:20)
  * ACTION: zack to ask SFLC / other project re making DMCA policy draft
    public  (zack, 17:39:44)
  * ACTION: algernon to review draft DMCA policy and adapt for the
    mentors.d.o use case  (algernon, 17:39:53)
  * ACTION: zack to write an outline of the -companies announcement and
    mail it to press@d.o  (zack, 17:40:43)
  * ACTION: lucas to finalize salvaging thread and prepare devref patch
    (zack, 17:44:21)
  * ACTION: lucas wrap-up the salvaging/orphaning thread and submit
    dev-ref patch. also address #681833.  (lucas, 17:44:22)
  * ACTION: Diziet to bug zack re next meeting agenda  (Diziet,
    17:44:55)
  * restricted logo has been renamed as such and is now live on the
    website  (zack, 17:44:58)

* next meeting  (zack, 17:45:15)
  * ACTION: zack to write an outline of the -companies announcement and
    mail it to press@d.o  (zack, 17:46:35)
  * Next meeting   Tue Nov  6 18:00:00 GMT 2012   date -d @1352224800
    (Diziet, 17:49:53)

* miscellanea  (zack, 17:50:13)

Meeting ended at 18:00:59 UTC.




Action Items
------------
* topic to raise at UDS: ask what Debian could do to avoid Ubuntu's need
  to fork packages
* zack (at UDS) to ask Ubuntu contributors to send a mail to inform
  relevant Debian maintainers when a UDS proposal affect their packages,
  so they know about it/ can provide feedback before implementation
  starts
* zack to draft a RT mail outline and pass it on to RT
* Diziet start conversation on -project about dpl statement process
* Diziet draft more formal statement re trademarks and discuss on
  -project, apropos of
  https://lists.debian.org/debian-project/2012/02/msg00073.html
* zack to ask SFLC / other project re making DMCA policy draft public
* algernon to review draft DMCA policy and adapt for the mentors.d.o use
  case
* zack to write an outline of the -companies announcement and mail it to
  press@d.o
* lucas to finalize salvaging thread and prepare devref patch
* lucas wrap-up the salvaging/orphaning thread and submit dev-ref patch.
  also address #681833.
* Diziet to bug zack re next meeting agenda
* zack to write an outline of the -companies announcement and mail it to
  press@d.o




Action Items, by person
-----------------------
* algernon
  * algernon to review draft DMCA policy and adapt for the mentors.d.o
    use case
* Diziet
  * Diziet start conversation on -project about dpl statement process
  * Diziet draft more formal statement re trademarks and discuss on
    -project, apropos of
    https://lists.debian.org/debian-project/2012/02/msg00073.html
  * Diziet to bug zack re next meeting agenda
* lucas
  * lucas to finalize salvaging thread and prepare devref patch
  * lucas wrap-up the salvaging/orphaning thread and submit dev-ref
    patch. also address #681833.
* zack
  * zack (at UDS) to ask Ubuntu contributors to send a mail to inform
    relevant Debian maintainers when a UDS proposal affect their
    packages, so they know about it/ can provide feedback before
    implementation starts
  * zack to draft a RT mail outline and pass it on to RT
  * zack to ask SFLC / other project re making DMCA policy draft public
  * zack to write an outline of the -companies announcement and mail it
    to press@d.o
  * Diziet to bug zack re next meeting agenda
  * zack to write an outline of the -companies announcement and mail it
    to press@d.o
* **UNASSIGNED**
  * topic to raise at UDS: ask what Debian could do to avoid Ubuntu's
    need to fork packages




People Present (lines said)
---------------------------
* zack (161)
* Diziet (27)
* lucas (27)
* Maulkin (23)
* algernon (20)
* moray (18)
* MeetBot (2)




Generated by `MeetBot`_ 0.1.4

.. _`MeetBot`: http://wiki.debian.org/MeetBot
