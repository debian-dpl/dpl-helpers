17:59:43 <zack> #startmeeting
17:59:43 <MeetBot> Meeting started Tue Jan 22 17:59:43 2013 UTC.  The chair is zack. Information about MeetBot at http://wiki.debian.org/MeetBot.
17:59:43 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
17:59:47 <zack> MeetBot: pingall dpl-helpers IRC meeting starting now
17:59:47 <MeetBot> dpl-helpers IRC meeting starting now
17:59:47 <MeetBot> algernon bgupta buxy Diziet gnugr gregoa hug KGB-0 KGB-1 KGB-2 lucas Maulkin MeetBot moray_ OdyX pabs taffit taffit_sud zack
17:59:47 <MeetBot> dpl-helpers IRC meeting starting now
17:59:56 <zack> please say hi if you're around
17:59:57 <moray_> hi
18:00:58 <zack> what a crowd :-)
18:01:20 <moray_> indeed
18:01:40 <zack> oh well, the first one is easy enough, no matter how many we are
18:01:43 <zack> #topic next meeting
18:01:46 <Maulkin> o/
18:01:51 <Diziet> Hi.
18:01:58 * zack waves
18:02:08 <zack> next meeting: date -d @1360087200 (February 05) ? any objection?
18:02:16 <Diziet> I might need to leave early.
18:02:20 <Diziet> (on the 5th)
18:02:31 <zack> ok
18:02:31 * Maulkin is Skiing, so may or may not be around
18:02:40 <zack> fwiw, is the week after FOSDEM
18:03:04 <zack> still doesn't seem worth the hassle of picking another date, right?
18:03:17 <Maulkin> Not really...
18:03:19 <lucas> (hi)
18:03:28 <lucas> no objection
18:03:31 <zack> ok, so:
18:03:31 <zack> #agreed next meeting date -d @1360087200 (February 05)
18:03:40 <zack> #topic action items from last meeting
18:03:57 <zack> we've even lost paultag, I'll query him
18:03:58 <lucas> (I'm on 3G, so it's possible I'll get disconnected)
18:04:10 <zack> #action paultag to do the README for the dpl magic ical script (for real [real])
18:04:34 <zack> Diziet: next one if yours, I've some q/comments unless you've updates in addition to what's been going on in the -project thread
18:04:35 <Maulkin> My google calendar replacement is unhappy(tm) with it having ';'s in :(
18:04:51 <zack> Maulkin: uh?
18:04:56 <Maulkin> zack: the ical URL.
18:05:08 <zack> Maulkin: ah, maybe you can set up some redirection, or a deb.li static URL?
18:05:10 <Diziet> zack: I have nothing much by way of updates.
18:05:17 <Maulkin> zack: it has ';' in it, and it thinks that's a SQL injection attack
18:05:32 <Diziet> zack: But TBH I see myself as your assistant so I if you say you see consensus then that's what we will go with.
18:05:43 <zack> Diziet: so, I wonder if you can comment on why you think we don't have consensus on the topics at the bottom of your mail
18:06:02 <Diziet> To answer that properly I'd have to go through the previous thread again.
18:06:08 <zack> Diziet: oh, but I don't want to push in any specific direction, if you think there is not, I'm genuinly curious of understanding why
18:06:29 <Diziet> I just went through the thread and those were things that either weren't discussed or which there seemed to be some inconclusive discussion of.
18:06:40 <zack> there was indeed one point not discussed
18:06:46 <Diziet> I think the right answer is probably rather to put them forward again more forcefully as "we think there is consensus on XYZ"
18:06:55 <zack> i.e. whether the marks, after unbranding, could stya in the archive
18:07:10 <zack> Diziet: agreed
18:07:29 <Diziet> But anyway thanks for your reply.
18:07:36 <Diziet> I shall reply and I think I can go forward...
18:07:37 <zack> Diziet: do you have pending additional comments to that?
18:07:54 <Diziet> #action Diziet to push on with trademark, adopting comments from zack
18:07:56 <Diziet> No, I don't
18:08:01 <zack> ok, one last, related to this
18:08:07 <Diziet> OK
18:08:14 <zack> I'm a bit lost about the discussion with Q_ on DPL statements
18:08:18 <Diziet> Yes, me too.
18:08:23 <zack> it occurred to me that an alternative might be a DEP
18:08:26 <Diziet> I think we should press on and see if anyone actually objects.
18:08:27 <Diziet> Oh god.
18:08:32 <zack> ok, never mind :)
18:09:07 <zack> I just meant as a place where to store a document, if there's agreement already, it'd be pointless to go through the steps
18:09:16 <Diziet> Ah.
18:09:28 <zack> DEP was meant to be = process + storage
18:09:33 <zack> (of the state of a discussion)
18:09:50 <Diziet> The problem is that it doesn't have a clear status transition decisionmaker.
18:10:05 <Diziet> But I think that's another discussion...
18:10:09 <zack> indeed
18:10:29 <zack> anyway, I agree with you that a page with "DPL thinks there's consensus on..." it's absolutely fine, constitution-wise
18:11:13 <zack> ok, let's move on, ball in Diziet camp
18:11:22 <zack> (with many thanks for the work so far, I really appreciate)
18:11:31 <zack> #action zack to update the DMCA draft directly with the identified changes rationale
18:11:36 <zack> (I've been lagging on that one)
18:11:50 <zack> #action zack to contact debconf-team to draft a "job description" for delegation
18:11:53 <zack> (ditto)
18:12:00 <zack> lucas: your turn to lag :-P
18:12:20 <lucas> ETOOBUSY, I need to send a VAC email, I think :/
18:12:25 <zack> argh!
18:12:55 <zack> but that's fine, of course, just re #action it (or possibly move it to the todo.txt, which is for more long term stuff)
18:13:24 <lucas> ok, will move it to todo.txt
18:14:01 <zack> (looks like today is gonna be a rather quick meeting)
18:14:12 <zack> #topic matters of the day
18:14:25 <zack> I've added 3 minor call for helps to the agenda
18:14:41 <zack> first one is for everyone, in about 1.5 months we're gonna start the new election process
18:14:48 <zack> +/- 1 week (right Maulkin?)
18:14:57 <zack> I've started repeating to people I won't run again
18:15:12 <Maulkin> Hah, well, vote.debian.org is currently broken, so it may be zack for life!
18:15:15 <zack> meaning: this is *really* the time to start "bothering" people you think would be good candidates to run
18:15:18 <zack> Maulkin: :-P
18:15:33 <zack> the risk of people deciding only last minute because "we have elections, already?" is better be avoided
18:15:43 <Maulkin> (Interesting constitutionally what happens if we just don't run an election...)
18:16:02 <zack> Maulkin: I guess it says to start elections "immediately" when once notices, right?
18:16:08 <zack> Maulkin: so you'll just have to run it by hand :-P
18:16:20 <Maulkin> Probably
18:16:58 <zack> ayway, this is very DPL-ish, finding good candidates is fundamental -- I'm doing that, w/o much success for now :-P
18:17:17 <zack> feel free to /query me if you've candidates to propose and you want me to approach them, whatever
18:17:23 <zack> but keep this in mind
18:17:28 <zack> ----------------------------------------
18:17:35 <Maulkin> Normally call for nominations happens at FOSDEM time
18:17:45 <zack> Maulkin: right, that too
18:18:04 <Maulkin> And we bully^Wconvince people to stand at FOSDEM
18:18:13 <zack> (with lots of beers around, which helps)
18:18:25 <zack> the other matter is M$ Azure
18:18:37 <zack> we now have Debian there https://lists.debian.org/debian-cloud/2013/01/msg00029.html
18:18:51 <zack> I think we should send out a press release mentioning Debian support on a multitude of public clouds
18:18:56 <Maulkin> zack: Put an outline together on titanpad or something (with quotes) and I can PR it.
18:19:01 <zack> ... I could use a volunteer to draft the PR with me
18:19:07 <zack> Maulkin: oh, nice!
18:19:17 <zack> #action zack to draft outline for the debian on public cloud release
18:19:34 <zack> #action Maulkin to expand zack's draft PR on public cloud into some meaningful text
18:20:00 <Maulkin> Debian reaches the for the sky with global public clouds
18:20:01 <zack> ----------------------------------------
18:20:04 <Maulkin> Or something
18:20:11 <zack> you're really into PR :)
18:20:38 <Maulkin> Two minor issues to report from me (for interest of others)
18:20:45 <zack> Maulkin: just a sec
18:20:49 <zack> last one and then it's all misc
18:20:54 <Maulkin> Cool
18:21:02 <zack> we need to document our "infra" at www.d.o/Teams/DPL
18:21:05 <zack> list, irc channel, etc
18:21:16 <zack> very minor, I was hoping to pass it to nhandler
18:21:19 <taffit> s/www/wiki/ probably
18:21:27 <zack> taffit: right, sorry
18:21:40 <zack> I'll try to trick nathan :), or do it directly
18:21:43 <zack> --------------------------------
18:21:44 <zack> Maulkin: all yours
18:22:28 <Maulkin> New bits from the RT is due out tomorrow
18:22:29 <Maulkin> http://titanpad.com/debian-release
18:22:47 <Maulkin> Secret (not really) preview!
18:22:59 <zack> right!
18:23:06 <zack> s/is joining/has joined/ maybe?
18:23:13 <Maulkin> Yeah.
18:23:42 <zack> live editing rocks :)
18:23:57 <zack> Maulkin: you know my pet peeve already, I mention it her for others to comment:
18:24:09 <zack> I think you should mention the forthcoming RT triaging, even if you're not *committing* to it
18:24:25 <zack> because otherwise people will just do silly things, like projecting the current RC bug count decrease ratio
18:24:28 <zack> or something
18:24:45 <zack> it's not *that* important, but it could help with morale
18:24:53 <Maulkin> They do that anyway.
18:25:23 <Maulkin> Plus, I mentioned it in the last bits...
18:25:38 <Maulkin> And I need *something* to put in the next one! :)
18:25:42 <zack> yeah, but now you're at the point where it's (almost) feasible
18:25:57 <zack> Maulkin: ok, _that_ is a killer argument :-P
18:26:04 * Maulkin nods
18:26:12 <zack> #topic miscellanea
18:26:34 <Maulkin> It's actually slightly less fatuous then that - having something important to announce makes me write a bits mail - jmw for example
18:26:58 <zack> anything else? (I've one last, but it's very minor)
18:26:59 <Maulkin> Also, I was looking at the RT delegation - I'm fairly sure we're delegates already, by having things delegated from others.
18:27:06 <moray> Maulkin: right, though I agree with zack that it's not ideal if some "Debian is really late again" meme goes round
18:27:21 <Maulkin> moray: Hey, I made LWN quote of the week :)
18:27:46 <zack> moray: yeah, and I see that happening already, even if I think that, honestly, there's no basis for it
18:28:17 <moray> Maulkin: oh?  haven't checked it recently
18:28:23 <Maulkin> The way to combat that is for people not to (for example) upload a new gcc just before we freeze.
18:28:38 <zack> Maulkin: I thought about point "we're delegates already", and I understand it. But in DDs mind the RT is really a separate entity. I think it'd be good to have it reflected at the delegation level too
18:28:52 <zack> Maulkin: how true...
18:29:22 <zack> I don't think it's sane to have RT "power" stem from the delegation of other teams
18:29:29 <moray> zack: though if it *is* under an existing delegation, constitutionally you might not be able to re-delegate it at the same time :)
18:29:44 <Maulkin> zack: Fair enough. May be interesting to try and codify it...
18:29:59 <moray> I agree that delegated delegations aren't ideal, though
18:30:04 <zack> yeah, that's in TODO already, although I've been lagging
18:30:17 <Maulkin> I think we're also going to be looking at this time based freeze too. Not entirely sure it's worked this time round.
18:30:17 <zack> Maulkin: maybe we can have a chat (i.e. on this channel) from here and next meeting and try that out?
18:30:39 <Maulkin> zack: Probably needs someone to draft an outline first :)
18:30:50 <zack> sure
18:31:35 <zack> anyway, my last point was asking who around here is coming to FOSDEM, in case we want to try a F2F feedback moment
18:31:40 <zack> I'm going, and I know lucas is too
18:31:41 <moray> Maulkin: I share the concern that's been expressed, that a lot of people seem to see releasing as someone else's problem
18:31:54 * Maulkin is not. As it's FOSDEM. And I'm skiing :)
18:32:01 <zack> eh :)
18:32:06 <moray> (and just assume that the wonderfully efficient release team will get on with it)
18:32:26 * Maulkin nods.
18:32:29 <moray> I'm not booked for FOSDEM yet, though I was considering it (might not be able to go anyway)
18:32:34 <zack> Maulkin: I think in terms of bugs count, that's is strictly correlated with the number of packages in the archive, more than with the release technique
18:32:48 <Maulkin> See the hastle I got about RC bugs and NMU delays.
18:32:54 <zack> Maulkin: the time-based freezes help the "diligent" teams and, imho more importatnly, upstreams
18:33:07 <Maulkin> 2Are we really meant to ping an RC bug every X days to say we're working on it?" <-- Yes, it's a bloody RC bug.
18:33:22 <moray> Maulkin: that's a wider problem with all bugs anyway
18:33:23 <Maulkin> But hey, I'm grumpy.
18:33:31 <zack> I guess we can have a public discussion about this as soon as wheezy is out of the door?
18:33:38 <moray> Maulkin: many maintainers (including me) are bad with sending any reply until it's fixed
18:33:57 <Maulkin> Yeah, I intend on asking for a release team sprint about a month or so after it anyway
18:34:02 <zack> also, part of the problem is: with 2 years release cycle, is very hard to have decent feedback loops about release techniques
18:34:15 <Maulkin> Again, with a call for feedback etc (Not that I got much mail last time...)
18:34:22 <zack> the risk of flip-flopping about 2 mutually exclusive alternatives "just because" is high
18:35:27 <zack> anything else for the meeting or can we stop here?
18:36:12 * Maulkin is done
18:36:23 <zack> #endmeeting