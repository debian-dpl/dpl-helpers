17:59:37 <nhandler> #startmeeting
17:59:37 <MeetBot> Meeting started Tue Mar 12 17:59:37 2013 UTC.  The chair is nhandler. Information about MeetBot at http://wiki.debian.org/MeetBot.
17:59:37 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
17:59:42 <nhandler> Maulkin: pingall DPL Helpers Meeting
17:59:45 <nhandler> err...
17:59:50 <nhandler> MeetBot: pingall DPL Helpers Meeting
17:59:50 <MeetBot> DPL Helpers Meeting
17:59:50 <MeetBot> algernon bgupta buxy Diziet gnugr gregoa hug KGB-0 KGB-1 KGB-2 lucas Maulkin MeetBot moray nhandler pabs paultag Q_ taffit taffit_sud zack
17:59:50 <MeetBot> DPL Helpers Meeting
17:59:56 <nhandler> Who is here today?
18:00:01 <paultag> o/
18:00:12 <Maulkin> Not me. I'm a figment of your imagination. Wooo.....
18:00:17 <nhandler> :)
18:00:24 <moray> cough
18:00:52 <nhandler> zach is unable to make the meeting today, so it will most likely go pretty fast.
18:01:18 <nhandler> The first 2 actions were zach's. Unless anyone knows anything about them, I will just re-action them to him
18:01:25 * Maulkin does
18:01:36 <nhandler> Maulkin: Care to give a status update?
18:01:42 <Maulkin> Sure, just on the press one
18:01:51 <nhandler> Go for it
18:02:29 <Maulkin> Basically been going back and forth a couple of times, but it's almost ready to go.Currently scheduled before Friday
18:02:32 <Maulkin> (done)
18:03:04 <nhandler> Awesome! Looking forward to it.
18:03:18 <nhandler> Anyone know anything about the debconf-team "job description"?
18:03:54 <moray> I don't think anything has happened since the last meeting on that
18:04:02 <nhandler> Ok, I'll re-action it
18:04:07 <nhandler> #action zack to contact debconf-team to draft a "job description" for delegation
18:04:36 <nhandler> lucas: You around?
18:05:41 <nhandler> #action lucas to wrap-up the salvaging/orphaning thread and submit dev-ref patch
18:05:56 <nhandler> moray: Next item is yours
18:06:09 <moray> right, summary is to reaction it
18:06:18 <moray> it's partly done, but needs more attention to turn into reality
18:06:51 <nhandler> Ok, think it might be done by the next meeting in 2 weeks?
18:06:57 <nhandler> #action moray to check with debconf team how/if to deal with debconf invited talks
18:06:58 <moray> not much point saying "yes" but then forgetting the topic :)
18:07:39 <moray> I will try, but I'm sensitive to a few different things happening in debconf-team with high priority than talks
18:07:55 <nhandler> Ok, fair enough. Next item is also (partially) yours about the mail on -sponsors-discuss
18:08:21 <moray> well, the action point in itself happened, we replied and there is a discussion happening
18:08:45 <moray> so let's mark it as done so we feel some progress :)
18:09:43 <nhandler> Great! Do you think any sort of additional action is needed at this point? Or just wait and let the discussion progress?
18:10:45 <moray> I think we could just wait (=disuss there) for now, while not forgetting the topic
18:11:38 <nhandler> Ok. We can add an item to the todo file to keep it on the radar
18:11:59 <nhandler> bgupta: You around?
18:14:07 <nhandler> I thought I read about some progress on this topic, but I'm not seeing seeing the relevant email at the moment. Anyone else know anything (re: trademark action)?
18:15:26 <nhandler> Ok, I guess I'll just re-action it and we can review it again at the next meeting
18:15:36 <nhandler> #action bgupta to write temple reply to trademark requests that don't actually need approval, as they are nominative uses of the trademark
18:16:19 <nhandler> The next action didn't have a person assigned to it. It was just to invite the DPL candidates to participate in this channel. Since algernon, lucas, and moray are all here already, I marked it as done :)
18:16:30 <moray> seems efficient
18:16:49 <nhandler> Yep, easiest and fastest action item
18:16:58 <nhandler> Diziet: You here (re: inbound trademark policy) ?
18:18:02 <Diziet> Hi.
18:18:03 <Diziet> Yes.
18:18:06 <Diziet> I haven't done anything.
18:18:18 <nhandler> Ok, want to re-action it for next time?
18:18:19 <Diziet> I propose to wait for the present furore to die down a bit.
18:18:22 <Diziet> Yes.
18:18:41 <Diziet> #action Diziet Progress inbound trademark policy
18:18:51 <nhandler> Sounds like a plan to me
18:19:08 <nhandler> The last item was also bgupta's. Anyone know if the ping happened last week?
18:20:11 <nhandler> #action bgupta to ping mishi
18:21:22 <nhandler> The last item on the agenda is deciding on the next meeting time. By my math, this should be date -d @1364320800 (March 26). Could someone double check and make sure that this does not change the time of the meeting for zack (due to DST or any other time changes)?
18:21:43 <nhandler> And does this time/date work for everyone?
18:21:52 <paultag> time should be fine for me if it's at around this time
18:22:07 <nhandler> paultag: It should be the same time as today's meeting for US people
18:22:16 <paultag> WORKSFORME :)
18:22:48 <nhandler> I /think/ it is the meeting after next where we possibly change by an hour to keep the time the same for zack
18:22:55 <paultag> ah ok.
18:23:41 <nhandler> #agreed Next meeting: date -d @1364320800 (March 26)
18:23:54 <nhandler> Anyone else have anything to discuss?
18:24:14 <Maulkin> Not from me
18:24:25 <paultag> dpl election is going on
18:24:28 <paultag> ask questions in -vote
18:24:34 <paultag> some bits about dpl helpers if no one knew
18:25:03 <gregoa> nhandler: DST change in europe should be on 2013-03-31
18:25:56 <nhandler> gregoa: Right, so meeting after next. Thanks for double checking. Also, new DPL term starts April 17, so we might have a change in meeting time after that date to accommodate their schedule
18:26:41 <nhandler> Thanks for a great (and fast) meeting everyone. I'll handle the post-meeting tasks sometime today.
18:26:51 <nhandler> #endmeeting