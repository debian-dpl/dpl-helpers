
On Thu, Jun 27, 2013 at 7:00 AM, Mishi Choudhary <mishi@softwarefreedom.org> wrote:
> Hi Brian,
>
>
> There were two pending issues from my end. My response to both are
> as follows:
>
> I. Logo registration Requirements:
>
> Clearly describe the mark as to what it depicts;

A spiral formed with the style of a paintbrush stroke.

A JPEG image of the mark is attached to this email.

> What's the exact color combination which you would like to claim;

We would like to claim in black and white, as a silhouette.

> Date of First Usage of the mark;

The earliest documented use we could find was 25 June 1999. The vote
to accept it as the Debian logo took place on 4 June 1999.

> Proof of advertising material, conference signage where the mark is
> on display in .jpeg form; Any specimens of logo usage, tags, labels,
> instruction manuals;

We've attached a picture from the LinuxTag event on 25 June 1999.
We've also attached a screenshot of the website as it was on 13
October 1999.

> Unrelated, I hope SPI, Inc or DEBIAN has copyright assignment from
> the creator of the logo.

SPI has the copyright assignment as of 1999.

> II. ICANN's Trademark Clearing House Service:

Based on our understanding, we do not wish to register for either of
the Trademark Clearing House services at his time.

Thanks and best regards,

Debian Trademark Team
