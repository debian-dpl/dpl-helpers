
Hi Mishi,


Our apologies for the radio silence, we've been busy with a
conference, and there are some open questions about how to handle
Debian related domains that have slowed things down. These questions
are towards the end of this email, but we have taken the liberty of
providing:

- a little background info,
- our aims in having trademarks, and
- our understanding of the relevant issues

to assist in understanding where we are coming from and what we are
trying to achieve.


* Background

In the past, when we didn't have a trademark policy and trademark
registration, we let most things go.

Our recent policy has been to try and make sure all registered
debian.* domains are held by debian Trusted Organizations (TOs), like
SPI. (A Debian trusted organization is basically a legally
incorporated entity, generally a non-profit, that is authorized to
hold assets on Debian's behalf, and has committed to following the
Debian Project Leader's instructions on using, spending, and
transferring of those assets.)

For domains that contain Debian in their name but are not simply
Debian.* (as an example, http://debiandiary.com/), we have been
reviewing the use requests on a case by case basis, and generally
approving them:

a. if the requests are coming from a Debian friendly person, and
b. they are using the domain to promote or support Debian for
non-commercial purposes.

When we authorize them, we are doing so with the email template that
you approved that requires them to display certain text, and informs
them that the approval may be revoked.  We of course log all use
requests.


* Our aims

Debian has just celebrated its 20th birthday. The work we are doing
now must protect and strengthen us over the next 20+ years.

We already put an enormous amount of work into the technical quality
of our operating system (vetting of new members, extensive automated
checks of packages, etc). Additionally, our discipline in following
the DFSG [1] provides huge benefits to our users and related
distributions.

All of this work speaks to the value (to us and others) of our
product, name and trademarks.

We want to ensure that our reputation and the marks associated with
our work and reputation are not weakened.


* Issues we are trying to avoid

From our understanding of TM law, we think there are the following
issues:

1. The use of a trademark by other parties may have the effect of
   destroying the trademark through abandonment.

2. Debian's TM registration(s) could be cancelled if there is rampant
   non-licensed use, especially where Debian knows about the use and
   doesn't take adequate measures to stop the use.

3. A license, without control over the nature and quality of goods
   represented (at the very least some sort of inspection of the goods
   or the quality parameters) can also have the effect of destroying
   the trademark. Please see this article on "naked licenses" [2]

4. Our current trademark is not registered in every country in the
   world, so perhaps our current focus should be on the
   countries/jurisdictions, where our mark is registered. (US - CN -
   EM - GB - JP). Unfortunately there are a good number of sites in
   these markets that are not held by Debian TOs, [3]

5. Some of the debian.* domains are held by friendly entities. It
   is possible that one or more of these groups could become trusted
   organizations at some point, but of today, we have no formal legal
   relationship with them. We are not sure how to deal with such cases
   properly.


* Strategy/policy options

One possibility that has been proposed has been to permit less popular
debian.* domains to be registered by third parties under similar terms
as Debian fan sites. These third parties could be either community
people or possibility commercial entities.
Optionally, we could require those entities to sign an agreement the
termination of which would require them to hand over control over
the domain.

This would avoid Debian TOs having to pay for all domain names.

Are there any legal risks associated with this approach?


* Questions

1. Are the issues outlined above relevant and correct?
2. What are the risks in permitting commercial (or non-commercial)
   entities hold debian.* domains?
   a. Do these risks change if they are in countries that we do/don't
      hold TM registrations?
3. What would be the bare minimum we need to do regarding domains to
   protect our trademark? Are there relevant examples where trademarks
   have been lost through others using domains?
4. When domains are held by others, we don't then need to pay for and
   manage the registration, but we also wish to avoid extra vigilant
   scrutiny of the domain. Is this reasonable?


Hopefully the above is clear enough for you to provide reasonable
answers. If you have further questions, please do not hesitate to
email to discuss.


Thanks very much,

Debian Trademark Team


[1] - http://www.debian.org/social_contract.html
      http://en.wikipedia.org/wiki/Debian_Free_Software_Guidelines
[2] - http://www.insidecounsel.com/2011/09/06/ip-the-bare-facts-on-naked-licensing
[3] - http://www.debian.cn/, and http://www.debian.or.jp/,
      http://www.debian.at/, http://www.debian.cz/, http://debian.nl/,
      http://www.debian.it/
