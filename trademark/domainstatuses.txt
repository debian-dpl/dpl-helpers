--------------------------------------------------
Domain statuses:
-------------------------------------------------- 
NOTE: debian.* domains are in a seperate file

Looks ok - Technically compliant, but should ask to list Trademark info.
---
debienna.at debienna@rerun.lefant.net

Fan license?:
---
debian-es.org: Debian-ES (Is this a Trusted Org?)
debian-by.org debian-dug-by@lists.debian.org
padebian.org info@padebian.org
debian-ce.org frolic@debian-ce.org
debiancolombia.org  jguevara@debiancolombia.org - list administrator from http://listas.debiancolombia.org/cgi-bin/mailman/listinfo/general
www.debianperu.org rudy@apesol.org.pe
debianbrasil.org debian-br-geral@lists.alioth.debian.org 
cr.debian.net listmaster@cr.debian.net
debian.org.ni  jimbodoors94@gmail.com, leogg@debian.org.ni, gjcantarero@gmail.com - list admins from http://mail.debian.org.ni/mailman/listinfo/debian-ni_debian.org.ni
debianchile.org hostmaster@debianchile.org
debian.mx rafael@bucio.com.mx
debian.org.sv emonge@debian.org.sv
debian.org.tr fatihcelik@btegitim.com

debianhelp.co.uk - has a note in footer, saying not part of debian project, but still uses debian trademark as part of domainname.
debianuruguay.org - no progress - looks abandoned, but no address and nothing obvious from whois
debiancuba.org - suspended page
debianizzati.org
debianuserforums.org

Violation-friendly contacted (waiting on):
---

Need to contact (Friendly):
---
http://debian-blog.org/
  Nimmervoll Daniel
  Street1:Vittorellistrasse 1
  City:Linz
  State/Province:AT
  Postal Code:4040
  Country:AT
  Phone:+43.6769355808
  Tech Email:daniel@nimmervoll.eu
  Skype: Senimmi

Confusing uses, that we need to contact:
---
debianhosting.org
debianhosting.com
http://www.debianit.com/
http://www.debian-hosting.info

Need to investigate:
---
debian-multimedia.org
http://www.debian-administration.org/articles/134

Weird logos to check with SFLC when things are slow:
---
http://www.h2nature.de/downloads/h2nano-iq-water-basic-prospekt-05.2012.pdf - PDF 404s - website claims to reboot the whole project, whatever it might be, in 62 days - RichiH 2013-06-24
http://www.aurorahp.co.uk/ - looks like definite infringement; changed color to green, turned 90 degrees counter-clock-wise - RichiH 2013-06-24
http://rhonda.spreadshirt.at/ - Double swirl, red & white, in Austrian colours, on a t-shirt. Affiliated with debian.at and run by rhonda@debian - RichiH 20130621
www.feyapidenetim.com - reported by Fatih Celik <fatihcelik@btegitim.com>


trademark@debian.org
